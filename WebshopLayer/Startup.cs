﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebshopLayer.Startup))]
namespace WebshopLayer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
