﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using WebshopLayer.Classes;
using WebshopLayer.Models;
using HashLib;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace WebshopLayer.Controllers
{
    public class CrudController : Controller
    {
        // GET: Crud

        IMapper mapper;
        ILogic logic;
        CrudModel model;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            logic = new RealLogic();
            model = new CrudModel();
            model.ProductModel = new ProductModel();
            model.LoginModel = new LoginModel();
            model.AddObject = new CustomerModel();
            model.ContactModel = new ContactModel();

            var newestproducts = logic.GetNewestProducts();
            model.NewestProducts = mapper.Map<IQueryable<BusinessLogic.Queries.ProductsLowPrice>, List<Models.ProductModel>>(newestproducts);

            var offproducts = logic.GetTwoGames();
            model.OffProducts = mapper.Map<IQueryable<BusinessLogic.Queries.ProductsLowPrice>, List<Models.ProductModel>>(offproducts);

            var bestproducts = logic.GetThreeGames();
            model.BestProducts = mapper.Map<IQueryable<BusinessLogic.Queries.ProductsLowPrice>, List<Models.ProductModel>>(bestproducts);

            var categorylist = logic.GetCategroies();
            model.CategoryList = mapper.Map<IQueryable<Data.Categories>, List<Models.CategoryModel>>(categorylist);

            var productlist = logic.GetProducts();
            model.ProductList = mapper.Map<IQueryable<Data.Products>, List<Models.ProductModel>>(productlist);

            var customerlist = logic.GetCustomers();
            model.CustomerList = mapper.Map<IQueryable<Data.Customers>, List<Models.CustomerModel>>(customerlist);

            var platformlist = logic.GetPlatforms();
            model.PlatformList = mapper.Map<IQueryable<Data.Platforms>, List<Models.PlatformModel>>(platformlist);

            var wishlistgames = logic.WishList();
            model.WishListGames = mapper.Map<IQueryable<BusinessLogic.Queries.GetWishlistGame>, List<Models.GetWishListGameModel>>(wishlistgames);

            var orders = logic.Order();
            model.Order = mapper.Map<IQueryable<BusinessLogic.Queries.GetOrdersAndCustomersAndProducts>, List<Models.GetOrdersAndCustomersAndProductsModel>>(orders);

            base.OnActionExecuting(filterContext);
        }
        private CustomerModel getCustomerModel(int id)
        {
            Data.Customers c = logic.GetOneCustomer(id);
            return mapper.Map<Data.Customers, Models.CustomerModel>(c);
        }

        private ProductModel getProductModel(int id)
        {
            Data.Products p = logic.GetOneProduct(id);
            return mapper.Map<Data.Products, Models.ProductModel>(p);
        }

        private List<ProductModel> getProducts(int id)
        {
            var p = logic.ProductsInPlatform(id);
            return mapper.Map<IQueryable<BusinessLogic.Queries.ProductsInPlatform>, List<Models.ProductModel>>(p);
        }
        private List<ProductModel> getProductsPriceLow()
        {
            var p = logic.ProductsLowPrice();
            return mapper.Map<IQueryable<BusinessLogic.Queries.ProductsLowPrice>, List<Models.ProductModel>>(p);
        }
        private List<ProductModel> getProductsPriceHigh()
        {
            var p = logic.ProductsHighPrice();
            return mapper.Map<IQueryable<BusinessLogic.Queries.ProductsHighPrice>, List<Models.ProductModel>>(p);
        }

        private List<ProductModel> searchProduct(string search)
        {
            var p = logic.GetProductsAndPlatformsAndCategories(search);
            return mapper.Map<IQueryable<BusinessLogic.Queries.GetProductsAndPlatformsAndCategories>, List<Models.ProductModel>>(p);
        }

        // Menüpontok, belépés nélkül elérhető
        public ActionResult Index()
        {
            ViewData["TargetAction"] = "AddToWishList";
            ViewData["TargetAction"] = "AddToCart";
            return View("CrudIndex", model);
        }

        public ActionResult Contact()
        {
            return View("CrudContact");
        }

        public ActionResult PlatformList(int id)
        {
            if (id == 100)
            {
                return View("CrudIndex", model);
            }
            model.ProductList = getProducts(id);
            return View("CrudIndex", model);
        }

        public ActionResult Details(int id)
        {
            ViewData["TargetAction"] = "AddToWishList";
            return View("CrudProductDetails", getProductModel(id));
        }

        public ActionResult Reg()
        {
            ViewData["TargetAction"] = "AddNew";
            return View("_CrudRegisterPartial", model);
        }
        public ActionResult LoginView()
        {
            return View("CrudLogin");
        }

        public ActionResult Show(int id)
        {
            var productlist = logic.GetProducts();
            foreach (var item in productlist)
            {
                if (id == item.ID)
                {
                    var imageData = item.image1;
                    return File(imageData, "image/jpg");
                }
            }
            return View("LoginErrorView");
        }

        public ActionResult PriceType(int id)
        {
            if (id == 1)
            {
                model.ProductList = getProductsPriceLow();
                return View("CrudIndex", model);
            }
            model.ProductList = getProductsPriceHigh();
            return View("CrudIndex", model);
        }

        public ActionResult Search(string search)
        {
            model.ProductList = searchProduct(search);
            return View("CrudIndex", model);
        }

        public ActionResult SendEmail(ContactModel model)
        {
            //Dictionary<string, FileStream> attachments = xxx;
            string targetAddr = "dicehtc@gmail.com";
            string senderAddr = model.E_mail;
            string subject = model.FullName;
            string body = model.Message;

            MailMessage mm = new MailMessage(senderAddr, targetAddr, subject, body);
            mm.SubjectEncoding = UTF8Encoding.UTF8;
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            //if (attachments != null)
            //{
            //    foreach (var akt in attachments)
            //    {
            //        mm.Attachments.Add(new Attachment(akt.Value, akt.Key));
            //    }
            //}
            ServicePointManager.ServerCertificateValidationCallback =
                    (sender, certificate, chain, sslPolicyErrors) => true;
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 100000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            //string password = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["SMTP_PASS"], Services.ComputerId);
            //client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_USER"], password);
            client.Credentials = new NetworkCredential("dicehtc@gmail.com", "jelszo");
            client.Send(mm);
            return View("CrudIndex");
        }

        // Belépés után elérhető
        // Rendelés
        [MyAuthorizeFilter]
        public ActionResult Cart()
        {
            return View("CrudCart", MyAuthorizeFilterAttribute.SessionStore);
        }

        [MyAuthorizeFilter]
        public ActionResult UpdateCartItem(FormCollection fc)
        {
            string[] quantities = fc.GetValues("quantity");
            var list = MyAuthorizeFilterAttribute.SessionStore;
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Quantity = Convert.ToInt32(quantities[i]);
            }
                Session["Cart"] = list;
                return RedirectToAction("Cart");
            
        }

        [MyAuthorizeFilter]
        public ActionResult RemoveCartItem(int id)
        {
            var list = MyAuthorizeFilterAttribute.SessionStore;
            try
            {
                list.Remove(id);
            }
            catch (ArgumentException)
            {
                return View("LoginErrorView");
            }           
            return RedirectToAction("Cart");
        }

        [MyAuthorizeFilter]
        public ActionResult AddToCart(int customerid, int productid, string productname, string description, int price)
        {
            var listmodel = MyAuthorizeFilterAttribute.SessionStore;
            GetOrdersAndCustomersAndProductsModel Cart = new GetOrdersAndCustomersAndProductsModel();
            int id=0;           
            Cart.CustomerID = customerid;
            Cart.ProductID = productid;
            Cart.ProductName = productname;
            Cart.Description = description;
            Cart.Price = price;                     
            Cart.OrderID++;
            Cart.Quantity = 1;
            Cart.SubTotal = Cart.Quantity * price;
            if (listmodel != null)
            {
                foreach (var item in listmodel)
                {
                    id++;
                    
                    if (Cart.CustomerID == item.Value.CustomerID && Cart.ProductID == item.Value.ProductID)
                    {
                        return View("LoginErrorView");
                    }
                }               
            }
            else
            {
                return View("LoginErrorView");
            }
            listmodel.Add(id,Cart);
            return RedirectToAction("Index");
        }

        // Wishlisthez hozzáadás/törlés

        [MyAuthorizeFilter]
        public ActionResult Wishlist()
        {
            return View("CrudWishlist", model.WishListGames);
        }
        [MyAuthorizeFilter]
        public ActionResult AddToOrder(OrderModel ordermodel, OrderProductsModel orderproductsmodel, int customerid)
        {
            customerid = ordermodel.CustomerID;
            Data.Orders o = mapper.Map<Models.OrderModel, Data.Orders>(ordermodel);
            Data.OrderProducts op = mapper.Map<Models.OrderProductsModel, Data.OrderProducts>(orderproductsmodel);
            var listmodel = MyAuthorizeFilterAttribute.SessionStore;
            var prodmodel = logic.GetProducts();
            o.OrderStateID = 1;
            logic.AddOrders(o);
            op.OrderID = logic.GetNextOrderId();            
            ordermodel.CustomerID = o.CustomerID;
            foreach (var item in listmodel)
            {
                op.ProductID = item.Value.ProductID;
                foreach (var itemprod in prodmodel)
                {
                    if (item.Value.Quantity > itemprod.Quantity || itemprod.Quantity == 0)
                    {
                        return View("LoginErrorView");
                    }
                    else if (item.Value.ProductID == itemprod.ID && itemprod.Quantity != 0)
                    {
                        itemprod.Quantity -= item.Value.Quantity;
                    }
                }

                op.Quantity = item.Value.Quantity;
                //logic.ModifyProduct();
                logic.AddOrderProducts(op);
            }
            
            listmodel.Clear();

            return View("Order");
        }

        [MyAuthorizeFilter]
        public ActionResult AddToWishList(WishListModel model, int customerid, int productid)
        {
            customerid = model.CustomerID;
            productid = model.ProductID;
            var wishlist = logic.GetWishList();
            Data.WishList w = mapper.Map<Models.WishListModel, Data.WishList>(model);
            foreach (var item in wishlist)
            {
                if (model.ProductID == item.ProductID && model.CustomerID == item.CustomerID)
                {
                    return View("LoginErrorView");
                }
            }
            model.ProductID = w.ProductID;
            model.CustomerID = w.CustomerID;
            logic.AddWishList(w);
            return RedirectToAction("Index");
        }

        [MyAuthorizeFilter]
        public ActionResult RemoveWishlistItem(int id)
        {
            try
            {
                logic.DelWishListItem(id);
                TempData["result"] = "Del OK";
            }
            catch (ArgumentException)
            {
                return View("LoginErrorView");
            }
            return RedirectToAction("WishList");
        }

        // Regisztráció/Belépés/Kilépés
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AddNew(CustomerModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                var User = logic.GetCustomers();
                Data.Customers d = mapper.Map<Models.CustomerModel, Data.Customers>(model);
                foreach (var item in User)
                {
                    if (model.E_mail == item.E_mail)
                    {
                        return View("LoginErrorView");
                    }
                }
                d.CreateDate = DateTime.Now;
                //IHash hash = HashFactory.Crypto.SHA3.CreateKeccak512();
                //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                //HashAlgorithm hashAlgo = HashFactory.Wrappers.HashToHashAlgorithm(hash);
                //HashResult r = hash.ComputeString(d.Password, System.Text.Encoding.ASCII);
                //d.Password = r.ToString();
                logic.AddCustomer(d);
                return RedirectToAction("LoginView");
            }
            return View("LoginErrorView");
        }


        public ActionResult Login(LoginModel model)
        {
            var User = logic.GetCustomers();

            //IHash hash = HashFactory.Crypto.SHA3.CreateKeccak512();
            //HashAlgorithm hashAlgo = HashFactory.Wrappers.HashToHashAlgorithm(hash);           

            foreach (var item in User)
            {
                //HashResult r = hash.ComputeString(model.Password, System.Text.Encoding.ASCII);
                if (model.E_mail == item.E_mail && item.Password == model.Password /*item.Password == r.ToString()*/)
                {
                    Session["UserId"] = item.ID;
                    Session["UserEmail"] = item.E_mail;
                    return RedirectToAction("Index");
                }
                else
                {
                    continue;
                }
            }
            return View("LoginErrorView");
        }

        public ActionResult Logout()
        {
            Session["UserId"] = null;
            Session.Abandon();
            return RedirectToAction("Index");
        }

        // Profil Szerkesztés

        [MyAuthorizeFilter]
        public ActionResult Profil(int id)
        {
            return View("CrudProfile", getCustomerModel(id));
        }

        [MyAuthorizeFilter]
        public ActionResult Edit(int id)
        {
            ViewData["TargetAction"] = "Edit";
            model.AddObject = getCustomerModel(id);
            return View("CrudEdit", getCustomerModel(id));
        }

        [MyAuthorizeFilter]
        [HttpPost]
        public ActionResult Edit(CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                logic.ModifyCustomer(model.ID, model.FirstName, model.LastName, model.Password, model.ConfirmPassword, model.PhoneNumber, model.ZipCode, model.Town, model.Street, model.StreetNumber);
                return RedirectToAction("Profil", new { id=model.ID });
            }
            else
            {
                return View("LoginErrorView");
            }
        }

        // Elfelejtett jelszó
        //[AllowAnonymous]
        //public ActionResult ForgotPassword()
        //{
        //    return View();
        //}



        //
        // POST: /Account/ForgotPassword
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ForgotPassword(CustomerModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = logic.GetOneCustomer(model.ID);
        //        if (user == null)
        //        {
        //            // Don't reveal that the user does not exist or is not confirmed
        //            return View("Kivánságlista");
        //        }

        //        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
        //        // Send an email with this link
        //        // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
        //        // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
        //        // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //        // return RedirectToAction("ForgotPasswordConfirmation", "Account");
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}


    }
}