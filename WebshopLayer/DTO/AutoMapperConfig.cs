﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebshopLayer.DTO
{
    class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Categories, DTO.Categories>().ReverseMap();
                cfg.CreateMap<Data.Categories, Models.CategoryModel>().ReverseMap();
                cfg.CreateMap<Data.Customers, DTO.Customers>().ReverseMap();
                cfg.CreateMap<Data.Customers, Models.CustomerModel>().ReverseMap();
                cfg.CreateMap<Data.Products, DTO.Products>().ReverseMap();
                cfg.CreateMap<Data.Products, Models.ProductModel>().ReverseMap();
                cfg.CreateMap<Data.Platforms, DTO.Platforms>().ReverseMap();
                cfg.CreateMap<Data.Platforms, Models.PlatformModel>().ReverseMap();
                cfg.CreateMap<Data.WishList, DTO.WishList>().ReverseMap();
                cfg.CreateMap<Data.WishList, Models.WishListModel>().ReverseMap();

                cfg.CreateMap<Data.Orders, Models.OrderModel>().ReverseMap();
                cfg.CreateMap<Data.OrderProducts, Models.OrderProductsModel>().ReverseMap();

                cfg.CreateMap<BusinessLogic.Queries.GetWishlistGame, DTO.GetWishListGame>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.GetWishlistGame, Models.GetWishListGameModel>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.GetOrdersAndCustomersAndProducts, DTO.GetOrdersAndCustomersAndProducts>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.GetOrdersAndCustomersAndProducts, Models.GetOrdersAndCustomersAndProductsModel>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsInPlatform, DTO.Products>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsInPlatform, Models.ProductModel>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsLowPrice, DTO.Products>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsLowPrice, Models.ProductModel>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsHighPrice, DTO.Products>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.ProductsHighPrice, Models.ProductModel>().ReverseMap();
                cfg.CreateMap<BusinessLogic.Queries.GetProductsAndPlatformsAndCategories, Models.ProductModel>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}