﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebshopLayer.DTO
{
    public class GetWishListGame
    {
        public int WishlistID { get; set; }
        public int GameID { get; set; }
        public int CustomerID { get; set; }
        public string GameName { get; set; }
        public int GamePrice { get; set; }
        public byte[] Image1 { get; set; }
    }
}