﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.DTO
{
    public class Products
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int AgeLimit { get; set; }
        public string Description { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int CategoryID { get; set; }
        public int PlatformID { get; set; }
        public byte[] image1 { get; set; }
    }
}