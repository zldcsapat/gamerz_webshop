﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.DTO
{
    public class Categories
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}