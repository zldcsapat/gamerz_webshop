﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebshopLayer.DTO
{
    public class Orders
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
    }
}