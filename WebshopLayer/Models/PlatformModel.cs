﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class PlatformModel
    {
        [Display(Name = "Platform ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Platform Name")]
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Name { get; set; }
    }
}