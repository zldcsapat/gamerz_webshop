﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class ProductModel
    {
        [Display(Name = "Product ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Product Name")]
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Name { get; set; }

        [Display(Name = "Agelimit")]
        [Required]
        public Decimal AgeLimit { get; set; }

        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "ReleaseDate")]
        [Required]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Ár")]
        [Required]
        public int Price { get; set; }

        [Display(Name = "Mennyiség")]
        [Required]
        public int Quantity { get; set; }

        [Display(Name = "CategoryID")]
        [Required]
        public int CategoryID { get; set; }

        [Display(Name = "PlatformID")]
        [Required]
        public int PlatformID { get; set; }

        [Display(Name = "image1")]
        [Required]
        public byte[] image1 { get; set; }
    }
}