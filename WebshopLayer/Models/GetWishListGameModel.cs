﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class GetWishListGameModel
    {
        [Display(Name = "Wishliust ID")]
        [Required]
        public int WishlistID { get; set; }

        [Display(Name = "Game ID")]
        [Required]
        public int GameID { get; set; }

        [Display(Name = "Customer ID")]
        [Required]
        public int CustomerID { get; set; }

        [Display(Name = "Game Name")]
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string GameName { get; set; }

        [Display(Name = "Leírás")]
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Description { get; set; }

        [Display(Name = "Ár")]
        [Required]
        public int GamePrice { get; set; }

        [Display(Name = "Kép")]
        [Required]
        public byte[] Image1 { get; set; }
    }
}