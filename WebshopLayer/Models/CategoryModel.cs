﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class CategoryModel
    {
        [Display(Name = "Category ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Category Name")]
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Name { get; set; }
    }
}