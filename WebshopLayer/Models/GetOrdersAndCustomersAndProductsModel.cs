﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class GetOrdersAndCustomersAndProductsModel
    {
        [Display(Name = "Orders ID")]
        [Required]
        public int OrderID { get; set; }

        [Display(Name = "Customer ID")]
        [Required]
        public int CustomerID { get; set; }

        [Display(Name = "Product ID")]
        [Required]
        public int ProductID { get; set; }

        [Display(Name = "Product Name")]
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string ProductName { get; set; }

        [Display(Name = "Leirás")]
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Description { get; set; }

        [Display(Name = "Ár")]
        [Required]
        public int Price { get; set; }

        [Display(Name = "Kép")]
        [Required]
        public byte[] image1 { get; set; }

        [Display(Name = "Részösszeg")]
        [Required]
        public int SubTotal { get; set; }

        [Display(Name = "Darabszám")]
        [Required]
        public int Quantity { get; set; }

        [Display(Name = "Végösszeg")]
        [Required]
        public int TotalPrice { get; set; }
    }
}