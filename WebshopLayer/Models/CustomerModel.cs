﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class CustomerModel
    {
        [Display(Name = "ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Keresztnév")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50, ErrorMessage = "Legalább 2 karatker és maximum 50!", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Display(Name = "Vezetéknév")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50, ErrorMessage = "Legalább 2 karatker és maximum 50!", MinimumLength = 2)]
        public string LastName { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [EmailAddress]
        [StringLength(50)]
        public string E_mail { get; set; }

        [Display(Name = "Jelszó")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Jelszó újra")]
        [Compare("Password", ErrorMessage = "A jelszavaknak egyeznie kell!")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Telefonszám")]
        [Phone(ErrorMessage ="A megadott adatnak telefonszámnak kell lennie!")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Irányitószám")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [Range(1000,9999, ErrorMessage = "Az irányótámsznak 4 számjegyűnek kell lennie!")]
        public int ZipCode { get; set; }

        [Display(Name = "Város")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50)]
        public string Town { get; set; }

        [Display(Name = "Utca")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50)]
        public string Street { get; set; }

        [Display(Name = "Házszám")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        public int StreetNumber { get; set; }

        [Display(Name = "CreateDate")]
        [Required]
        public DateTime CreateDate { get; set; }
    }
}