﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class WishListModel
    {
        [Display(Name = "WishList ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Customer ID")]
        [Required]
        public int CustomerID { get; set; }

        [Display(Name = "Product ID")]
        [Required]
        public int ProductID { get; set; }
    }
}