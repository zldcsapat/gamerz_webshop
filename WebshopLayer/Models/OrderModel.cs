﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class OrderModel
    {
        [Display(Name = "Order ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Customer ID")]
        [Required]
        public int CustomerID { get; set; }

        [Display(Name = "OrderState ID")]
        [Required]
        public int OrderStateID { get; set; }
    }
}