﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class ContactModel
    {
        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "A mező nem maradhat üresen")]
        [EmailAddress(ErrorMessage = "E-mail formátum megadása kötelező.")]
        [StringLength(50)]
        public string E_mail { get; set; }

        [Display(Name = "Teljes név")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(50, ErrorMessage = "Legalább 2 karatker és maximum 50!", MinimumLength = 2)]
        public string FullName { get; set; }

        [Display(Name = "Üzenet")]
        [Required(ErrorMessage = "A mező nem maradhat üresen!")]
        [StringLength(200, ErrorMessage = "Legalább 2 karatker és maximum 200!", MinimumLength = 2)]
        public string Message { get; set; }
    }
}