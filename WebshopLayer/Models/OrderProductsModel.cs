﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class OrderProductsModel
    {
        [Display(Name = "OrderProducts ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Product ID")]
        [Required]
        public int ProductID { get; set; }

        [Display(Name = "Order ID")]
        [Required]
        public int OrderID { get; set; }

        [Display(Name = "Mennyiség")]
        [Required]
        public int Quantity{ get; set; }
    }
}