﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class LoginModel
    {
        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "A mező nem maradhat üresen")]
        [EmailAddress(ErrorMessage = "E-mail formátum megadása kötelező.")]
        [StringLength(50)]
        public string E_mail { get; set; }

        [Display(Name = "Jelszó")]
        [Required(ErrorMessage = "A mező nem maradhat üresen")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
    }
}