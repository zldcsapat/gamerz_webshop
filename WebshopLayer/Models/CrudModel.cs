﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebshopLayer.Models
{
    public class CrudModel
    {
        public List<ProductModel> NewestProducts { get; set; }
        public List<ProductModel> OffProducts { get; set; }
        public List<ProductModel> BestProducts { get; set; }
        public WishListModel WishList { get; set; }
        public List<WishListModel> WishListList { get; set; }
        public List<GetOrdersAndCustomersAndProductsModel> Order { get; set; }
        public List<GetWishListGameModel> WishListGames { get; set; }
        public List<CategoryModel> CategoryList { get; set; }
        public List<ProductModel> ProductList { get; set; }
        public List<CustomerModel> CustomerList { get; set; }
        public List<PlatformModel> PlatformList { get; set; }
        public CustomerModel AddObject { get; set; }
        public LoginModel LoginModel { get; set; }
        public ContactModel ContactModel { get; set; }
        public ProductModel ProductModel { get; set; }
    }
}