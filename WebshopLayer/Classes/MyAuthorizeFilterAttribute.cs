﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebshopLayer.Models;

namespace WebshopLayer.Classes
{
    public class MyAuthorizeFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            object obj = filterContext.HttpContext.Session["UserId"];
            object obj2 = filterContext.HttpContext.Session["UserEmail"];
            object obj3 = filterContext.HttpContext.Session["Menu"];           

            if (obj == null)
            {
                filterContext.Result = new ContentResult() { Content = "ERROR 403" };
            }
        }

        public static Dictionary<int, GetOrdersAndCustomersAndProductsModel> SessionStore
        {
            get
            {
                if (HttpContext.Current.Session["Cart"] == null)
                {
                    HttpContext.Current.Session["Cart"] = new Dictionary<int, GetOrdersAndCustomersAndProductsModel>();
                }
                return HttpContext.Current.Session["Cart"] as Dictionary<int, GetOrdersAndCustomersAndProductsModel>;
            }
        }
    }
}
