﻿using System.Web;
using System.Web.Optimization;

namespace WebshopLayer
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/assets/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/ItemSlider").Include(
                        "~/assets/ItemSlider/js/modernizr.custom.63321.js",
                        "~/assets/ItemSlider/js/jquery.catslider.js,",
                        "~/assets/ItemSlider/css/demo.css",
                        "~/assets/ItemSlider/css/main-style.css"));



            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/assets/js/bootstrap.js",
                      "~/assets/js/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/assets/css").Include(
                      "~/assets/css/bootstrap.css",
                      "~/assets/css/style.css",
                      "~/assets/css/font-awesome.min.css"));

        }
    }
}
