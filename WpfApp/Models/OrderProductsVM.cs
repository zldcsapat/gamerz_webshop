﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class OrderProductsVM : ObservableObject
    {
        int iD;
        string productName;
        DateTime productReleaseDate;
        int productPrice;
        int productQuantity;
        byte[] productImage;
        string platformName;

        public int ID { get => iD; set =>Set(ref iD,value); }
        public string ProductName { get => productName; set => Set(ref productName, value); }
        public DateTime ProductReleaseDate { get => productReleaseDate; set => Set(ref productReleaseDate, value); }
        public int ProductPrice { get => productPrice; set => Set(ref productPrice, value); }
        public int ProductQuantity { get => productQuantity; set => Set(ref productQuantity, value); }
        public byte[] ProductImage { get => productImage; set => Set(ref productImage, value); }
        public string PlatformName { get => platformName; set => Set(ref platformName, value); }

        public OrderInformationsVM Clone()
        {
            return MemberwiseClone() as OrderInformationsVM;
        }
    }
}
