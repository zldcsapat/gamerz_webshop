﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class PlatformsVM : ObservableObject
    {
        int id;
        string name;

        public int Id { get => id; set => Set(ref id,value); }
        public string Name { get => name; set => Set(ref name,value); }

        public override string ToString()
        {
            return Name;
        }

        public PlatformsVM Clone()
        {
            return MemberwiseClone() as PlatformsVM;
        }

        public void CopyDataFrom(PlatformsVM other)
        {
            this.Id = other.Id;
            this.Name = other.Name;
        }
    }
}
