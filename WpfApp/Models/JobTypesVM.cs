﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models.JobTypes
{
    public class JobTypesVM : ObservableObject
    {
        int id;
        string name;

        public int Id { get => id; set => Set(ref id, value); }
        public string Name { get => name; set => Set(ref name,value); }

        public JobTypesVM Clone()
        {
            return MemberwiseClone() as JobTypesVM;
        }

        public void CopyDataFrom(JobTypesVM other)
        {
            this.Id = other.Id;
            this.Name = other.Name;
        }


    }
}
