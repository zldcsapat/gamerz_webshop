﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class OrderStateVM : ObservableObject
    {
        int iD;
        string name;

        public int ID { get => iD; set => Set(ref iD,value); }
        public string Name { get => name; set => Set(ref name,value); }

        public override string ToString()
        {
            return Name;
        }

        public OrderStateVM Clone()
        {
            return MemberwiseClone() as OrderStateVM;
        }

        public void CopyDataFrom(OrderStateVM other)
        {
            this.ID = other.ID;
            this.Name = other.Name;
        }
    }
}
