﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApp.Models.Products
{
    public class ProductsVM : ObservableObject
    {
        int id;
        string name;
        int agelimit;
        string description;
        DateTime releasedate;
        DateTime deletedate;
        int price;
        int quantity;
        int platformid;
        byte[] image1;
        byte[] image2;

        public int Id { get => id; set => Set(ref id,value); }
        public string Name { get => name; set => Set(ref name, value); }
        public int Agelimit { get => agelimit; set => Set(ref agelimit, value); }
        public string Description { get => description; set => Set(ref description, value); }
        public DateTime Releasedate { get => releasedate; set => Set(ref releasedate, value); }
        public DateTime Deletedate { get => deletedate; set => Set(ref deletedate, value); }
        public int Price { get => price; set => Set(ref price, value); }
        public int Quantity { get => quantity; set => Set(ref quantity, value); }
        public int Platformid { get => platformid; set => Set(ref platformid, value); }
        public byte[] Image1 { get => image1; set => Set(ref image1, value); }
        public byte[] Image2 { get => image2; set => Set(ref image2, value); }

        public ProductsVM Clone()
        {
            return MemberwiseClone() as ProductsVM; 
        }

        public void CopyDataFrom(ProductsVM other)
        {
            this.Id = other.Id;
            this.Name = other.Name;
            this.Agelimit = other.Agelimit;
            this.Description = other.Description;
            this.Releasedate = other.Releasedate;
            this.Deletedate = other.Deletedate;
            this.Price = other.Price;
            this.Quantity = other.Quantity;
            this.Platformid = other.Platformid;
            this.Image1 = other.Image1;
            this.Image2 = other.Image2;
        }
    }
}
