﻿using AutoMapper;
using BusinessLogic.Queries;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //USER
                cfg.CreateMap<Data.Users, UsersVM>().ReverseMap();

                //JobTypes
                cfg.CreateMap<Data.JobTypes, JobTypes.JobTypesVM>().ReverseMap();

                //Platforms
                cfg.CreateMap<Data.Platforms, Models.PlatformsVM>().ReverseMap();

                //Categories
                cfg.CreateMap<Data.Categories, Models.CategoriesVM>().ReverseMap();

                //PRODUCT!!!!!!!!!!!!!!!!!!!!!! DALMA :)
                cfg.CreateMap<Data.Products, Products.ProductsVM>().ReverseMap();

                // USERANDJOBTYPES
                cfg.CreateMap<BusinessLogic.Queries.GetUsersAndJobTypes, Models.UsersAndJobtypes.UsersAndJobTypesVM>().ReverseMap();
                cfg.CreateMap<Data.Users, Models.UsersAndJobtypes.UsersAndJobTypesVM>().ReverseMap();

                //PRODANDPLATANDCAT
                cfg.CreateMap<BusinessLogic.Queries.GetProductsAndPlatformsAndCategories, Models.ProductsAndPlatformsAndCategoriesVM>().ReverseMap();
                cfg.CreateMap<Data.Products, Models.ProductsAndPlatformsAndCategoriesVM>().ReverseMap();

                //OrderStates
                cfg.CreateMap<Data.OrderState, Models.OrderStateVM>().ReverseMap();

                //OrderInfos
                cfg.CreateMap<BusinessLogic.Queries.GetOrderInformations, Models.OrderInformationsVM>().ReverseMap();

                //OrderProds
                cfg.CreateMap<BusinessLogic.Queries.GetOrderProducts, Models.OrderProductsVM>().ReverseMap();

            });
            return config.CreateMapper();
        }
    }
}
