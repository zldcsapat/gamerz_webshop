﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class CategoriesVM : ObservableObject
    {
        int id;
        string name;

        public int Id { get => id; set => Set(ref id,value); }
        public string Name { get => name; set => Set(ref name, value); }

        public override string ToString()
        {
            return Name;
        }

        public CategoriesVM Clone()
        {
            return MemberwiseClone() as CategoriesVM;
        }

        public void CopyDataFrom(CategoriesVM other)
        {
            this.Id = other.Id;
            this.Name = other.Name;
        }
    }
}
