﻿using System;
using BusinessLogic;
using GalaSoft.MvvmLight;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace WpfApp.Models
{
    public class UsersVM : ObservableObject 
    {
        int id;
        string firstname;
        string lastname;
        string e_mail;
        string password;
        int? jobtypeid;

        public int Id { get => id; set => Set(ref id, value); }
        public string Firstname { get => firstname; set => Set(ref firstname, value); }
        public string Lastname { get => lastname; set => Set(ref lastname, value); }
        public string E_mail { get => e_mail; set => Set(ref e_mail, value); }
        public string Password { get => password; set => Set(ref password, value); }
        public int? JobTypeID { get => jobtypeid; set => Set(ref jobtypeid, value); }


        public UsersVM Clone()
        {
            return MemberwiseClone() as UsersVM;
        }

        public void CopyDataFrom(UsersVM other)
        {
            this.Firstname = other.Firstname;
            this.Lastname = other.Lastname;
            this.E_mail = other.E_mail;
            this.Password = other.Password;
            this.JobTypeID = other.JobTypeID;
        }
    }
}