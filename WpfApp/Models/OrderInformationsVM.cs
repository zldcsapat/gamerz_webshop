﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class OrderInformationsVM : ObservableObject
    {
        int orderID;
        int orderStateID;
        string orderStateName;
        string cusFirstName;
        string cusLastName;
        string cusEmail;
        string cusPhone;
        int cusZipCode;
        string cusTown;
        string cusStreet;
        int cusStreetNumber;


        public int OrderID { get => orderID; set => Set(ref orderID,value); }
        public int OrderStateID { get => orderStateID; set => Set(ref orderStateID,value); }
        public string OrderStateName { get => orderStateName; set => Set(ref orderStateName, value); }
        public string CusFirstName { get => cusFirstName; set => Set(ref cusFirstName, value); }
        public string CusLastName { get => cusLastName; set => Set(ref cusLastName, value); }
        public string CusEmail { get => cusEmail; set => Set(ref cusEmail, value); }
        public string CusPhone { get => cusPhone; set => Set(ref cusPhone, value); }
        public int CusZipCode { get => cusZipCode; set => Set(ref cusZipCode, value); }
        public string CusTown { get => cusTown; set => Set(ref cusTown, value); }
        public string CusStreet { get => cusStreet; set => Set(ref cusStreet, value); }
        public int CusStreetNumber { get => cusStreetNumber; set => Set(ref cusStreetNumber, value); }

        public OrderInformationsVM Clone()
        {
            return MemberwiseClone() as OrderInformationsVM;
        }
    }
}
