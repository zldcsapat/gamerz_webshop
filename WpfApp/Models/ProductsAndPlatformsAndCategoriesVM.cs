﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class ProductsAndPlatformsAndCategoriesVM : ObservableObject, IDataErrorInfo
    {
        int iD;
        string name;
        int ageLimit;
        string description;
        DateTime releaseDate;
        int price;
        int quantity;
        int categoryID;
        int platformID;
        byte[] image1;
        string platformName;
        string categoryName;

        public int ID { get => iD; set => Set(ref iD,value); }
        public string Name { get => name; set => Set(ref name, value); }
        public int AgeLimit { get => ageLimit; set => Set(ref ageLimit, value); }
        public string Description { get => description; set => Set(ref description, value); }
        public DateTime ReleaseDate { get => releaseDate; set => Set(ref releaseDate, value); }
        public int Price { get => price; set => Set(ref price, value); }
        public int Quantity { get => quantity; set => Set(ref quantity, value); }
        public int CategoryID { get => categoryID; set => Set(ref categoryID, value); }
        public int PlatformID { get => platformID; set => Set(ref platformID, value); }
        public byte[] Image1 { get => image1; set => Set(ref image1, value); }
        public string PlatformName { get => platformName; set => Set(ref platformName, value); }
        public string CategoryName { get => categoryName; set => Set(ref categoryName, value); }

        public ProductsAndPlatformsAndCategoriesVM Clone()
        {
            return MemberwiseClone() as ProductsAndPlatformsAndCategoriesVM;
        }

        public void CopyDataFrom(ProductsAndPlatformsAndCategoriesVM other)
        {
            this.ID = other.ID;
            this.Name = other.Name;
            this.AgeLimit = other.AgeLimit;
            this.Description = other.Description;
            this.ReleaseDate = other.ReleaseDate;
            this.Price = other.Price;
            this.Quantity = other.Quantity;
            this.CategoryID = other.CategoryID;
            this.PlatformID = other.PlatformID;
            this.Image1 = other.Image1;
            this.PlatformName = other.PlatformName;
            this.CategoryName = other.CategoryName;
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string propertyName]
        {
            get
            {
                RaisePropertyChanged("IsValid");
                return GetValidationError(propertyName);

            }
        }

        #region Validation
        static readonly string[] ValidatedProperties =
   {
            nameof(Name),
            nameof(AgeLimit),
            nameof(Description),
            nameof(ReleaseDate),
            nameof(Price),
            nameof(Quantity),
            nameof(CategoryID),
            nameof(PlatformID)
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                        return false;
                }
                return true;
            }
        }

        string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case nameof(Name):
                    error = ValidateName();
                    break;
                case nameof(AgeLimit):
                    error = ValidateAgeLimit();
                    break;
                case nameof(Description):
                    error = ValidateDescription();
                    break;
                case nameof(ReleaseDate):
                    error = ValidateReleaseDate();
                    break;
                case nameof(Price):
                    error = ValidatePrice();
                    break;
                case nameof(Quantity):
                    error = ValidateQuantity();
                    break;
                case nameof(CategoryID):
                    error = ValidateCategory();
                    break;
                case nameof(PlatformID):
                    error = ValidatePlatform();
                    break;
            }
            return error;
        }

        private string ValidatePlatform()
        {
            if (PlatformID == 0)
            {
                return "Adjon meg egy platformot a listából!";
            }
            return null;
        }

        private string ValidateCategory()
        {
            if(CategoryID == 0)
            {
                return "Adjon meg egy kategóriát a listából!";
            }
            return null;
        }

        private string ValidateQuantity()
        {
            if(Quantity < 1)
            {
                return "Az érték nem lehet kisebb 1-nél!";
            }
            return null;
        }

        private string ValidatePrice()
        {
            if (Price < 1)
            {
                return "Az érték nem lehet kisebb 1-nél!";
            }
            return null;
        }

        private string ValidateReleaseDate()
        {
            if (ReleaseDate == null)
            {
                return "Válasszon megjelenési dátumot!";
            }
            return null;
        }

        private string ValidateDescription()
        {
            if (String.IsNullOrWhiteSpace(Description))
            {
                return "A leírás mező nem lehet üres!";
            }
            return null;
        }

        private string ValidateAgeLimit()
        {
            if (AgeLimit < 1)
            {
                return "Az érték nem lehet kisebb 1-nél!";
            }

            return null;
        }

        private string ValidateName()
        {
            if (String.IsNullOrWhiteSpace(Name))
            {
                return "A Termék neve mező nem lehet üres!";
            }
            return null;
        }
        #endregion

    }
}
