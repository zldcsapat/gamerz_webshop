﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WpfApp.Models.JobTypes;

namespace WpfApp.Models.UsersAndJobtypes
{
    public class UsersAndJobTypesVM : ObservableObject, IDataErrorInfo
    {
        private Regex emailRegex = new Regex(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");
        private Regex letterRegex = new Regex(@"^[A-ZÍÉÁŰÚŐÓÜÖÄ][a-zíiűáéúőóüöä-]+$");
        private Regex phoneRegex = new Regex(@"^\+? (\d[\d -. ] +) ? (\([\d -. ]+\))?[\d-. ]+\d$");

        int iD;
        string firstname;
        string lastname;
        string e_mail;
        string password;
        int jobTypeID;
        string jobTypeName;

        public int ID { get => iD; set => Set(ref iD,value); }
        public string Firstname { get => firstname; set => Set(ref firstname, value); }
        public string Lastname { get => lastname; set => Set(ref lastname, value); }
        public string E_mail { get => e_mail; set => Set(ref e_mail, value); }
        public string Password { get => password; set => Set(ref password, value); }
        public int JobTypeID { get => jobTypeID; set => Set(ref jobTypeID, value); }
        public string JobTypeName { get => jobTypeName; set => Set(ref jobTypeName, value); }

        public UsersAndJobTypesVM Clone()
        {
            return MemberwiseClone() as UsersAndJobTypesVM;
        }

        public void CopyDataFrom(UsersAndJobTypesVM other)
        {
            this.ID = other.ID;
            this.Firstname = other.Firstname;
            this.Lastname = other.Lastname;
            this.E_mail = other.E_mail;
            this.Password = other.Password;
            this.JobTypeID = other.JobTypeID;
            this.JobTypeName = other.JobTypeName;
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string propertyName]
        {
            get
            {
                RaisePropertyChanged("IsValid");
                return GetValidationError(propertyName);

            }
        }

        #region Validation
        static readonly string[] ValidatedProperties =
   {
            nameof(Firstname),
            nameof(Lastname),
            nameof(E_mail),
            nameof(Password),
            nameof(JobTypeID)
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                        return false;
                }
                return true;
            }
        }

        string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case nameof(Firstname):
                    error = ValidateFirstname();
                    break;
                case nameof(Lastname):
                    error = ValidateLastname();
                    break;
                case nameof(E_mail):
                    error = ValidateEmail();
                    break;
                case nameof(Password):
                    error = ValidatePassword();
                    break;
                case nameof(JobTypeID):
                    error = ValidateJobTypeID();
                    break;
            }
            return error;
        }

        private string ValidateJobTypeID()
        {
            if(JobTypeID == 0)
            {
                return "Válasszon munkakört a listából!";
            }
            return null;
        }

        private string ValidatePassword()
        {
            if (String.IsNullOrWhiteSpace(Password))
            {
                return "A Jelszó mező nem lehet üres!";
            }
            if (Password.Length < 6)
            {
                return "A jelszónak legalább 6 karakter hosszúnak kell lennie!";
            }
            return null;
        }

        private string ValidateEmail()
        {
            if (String.IsNullOrWhiteSpace(E_mail))
            {
                return "Az E-mail mező nem lehet üres!";
            }
            if (!emailRegex.IsMatch(E_mail))
            {
                return "Adjon meg megfelelő felépítésű E-mailt\nPl.: pelda@valami.hu";
            }
            return null;
        }

        private string ValidateLastname()
        {
            if (String.IsNullOrWhiteSpace(Lastname))
            {
                return "A Keresztnév mező nem lehet üres!";
            }
            if (!letterRegex.IsMatch(Lastname))
            {
                return "Adjon meg megfelelő felépítésű Keresztnevet!\nPl.: Béla";
            }

            return null;
        }

        private string ValidateFirstname()
        {
            if (String.IsNullOrWhiteSpace(Firstname))
            {
                return "A Vezetéknév nem lehet üres!";
            }
            if (!letterRegex.IsMatch(Firstname))
            {
                return "Adjon meg megfelelő felépítésű Vezetéknevet!\nPl.: Kovács";
            }
            return null;
        }
        #endregion
    }
}
