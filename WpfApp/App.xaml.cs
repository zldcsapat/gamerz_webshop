﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfApp.Models;
using WpfApp.Services;
using WpfApp.ViewModel;
using WpfApp.Views;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);


            //User
            SimpleIoc.Default.Register<IUserViewModel, UserViewModel>();
            SimpleIoc.Default.Register<IUserViewLogic, UserViewLogic>();

            //EditUser
            SimpleIoc.Default.Register<IEditUsersViewModel, EditUsersViewModel>();
            SimpleIoc.Default.Register<IEditUsersLogic, EditUsersLogic>();

            //Main
            SimpleIoc.Default.Register<IMainViewModel, MainViewModel>();
            SimpleIoc.Default.Register<IMainViewLogic, MainViewLogic>();

            //Product
            SimpleIoc.Default.Register<IProductsViewModel, ProductsViewModel>();
            SimpleIoc.Default.Register<IProductsViewLogic, ProductsViewLogic>();

            //EditProduct
            SimpleIoc.Default.Register<IEditProductsViewModel, EditProductsViewModel>();
            SimpleIoc.Default.Register<IEditProductsLogic, EditProductsLogic>();

            //Login
            SimpleIoc.Default.Register<ILoginViewModel, LoginViewModel>();
            SimpleIoc.Default.Register<ILoginViewLogic, LoginViewLogic>();

            //Orders
            SimpleIoc.Default.Register<IOrdersViewModel, OrdersViewModel>();
            SimpleIoc.Default.Register<IOrdersViewLogic, OrdersViewLogic>();

            //ShowOrders
            SimpleIoc.Default.Register<IShowOrdersViewModel, ShowOrdersViewModel>();
            SimpleIoc.Default.Register<IShowOrdersLogic, ShowOrdersLogic>();

            SimpleIoc.Default.Register<ILogic>(() => new RealLogic()); // PreferredConstructor
            SimpleIoc.Default.Register<IMapper>(() => AutoMapperConfig.GetMapper()); // must have
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.ShowDialog();

            //MainWindow m = new MainWindow();
            //m.Show();

        }
    }
}
