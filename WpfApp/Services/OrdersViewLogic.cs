﻿using AutoMapper;
using BusinessLogic;
using BusinessLogic.Queries;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.ViewModel;
using WpfApp.Views;

namespace WpfApp.Services
{
    class OrdersViewLogic : IOrdersViewLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        IOrdersViewModel OrderVM
        {
            get { return ServiceLocator.Current.GetInstance<IOrdersViewModel>(); }
        }

        public void DelCmd(OrderInformationsVM order)
        {
            if (order == null)
            {
                Messenger.Default.Send(new NotificationMessage("No Order Selected!"));
                return;
            }
            if(order.OrderStateID != 3)
            BusinessLogic.ModifyOrder(order.OrderID,3);
        }
        public void ShowCmd(OrderInformationsVM order)
        {
            if (order == null)
            {
                Messenger.Default.Send(new NotificationMessage("No Order Selected!"));
                return;
            }
            ShowOrdersWindow sow = new ShowOrdersWindow(order);
            sow.ShowDialog();
        }

        public ObservableCollection<OrderInformationsVM> GetOrderList(string txt,int orderstateid)
        {
            var ordersDB = BusinessLogic.GetOrderInformations(txt,orderstateid).ToList();
            return Mapper.Map<List<GetOrderInformations>, ObservableCollection<Models.OrderInformationsVM>>(ordersDB);
        }

        public ObservableCollection<OrderStateVM> GetOrderStates()
        {
            var orderstatesDB = BusinessLogic.GetOrderStates().ToList();
            return Mapper.Map<List<Data.OrderState>, ObservableCollection<Models.OrderStateVM>>(orderstatesDB);
        }
    }
}
