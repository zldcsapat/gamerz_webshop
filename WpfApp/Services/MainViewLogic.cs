﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.ViewModel;
using WpfApp.Views;

namespace WpfApp.Services
{
    class MainViewLogic : IMainViewLogic
    {
        public void LogOutCmd(object obj)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
            var mw = obj as MainWindow;
            mw?.Close();

        }
    }
}
