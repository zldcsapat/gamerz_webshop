﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Services
{
    interface IShowOrdersLogic
    {
        void CloseCmd(object obj);
        ObservableCollection<Models.OrderProductsVM> GetOrderedProductList(int orderid);

    }
}
