﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Services
{
    interface IEditProductsLogic
    {
        bool EditProduct(Models.ProductsAndPlatformsAndCategoriesVM p);
        ObservableCollection<Models.PlatformsVM> GetPlatformsList();
        ObservableCollection<Models.CategoriesVM> GetCategoriesList();

        void UploadCmd(object img);
    }
}
