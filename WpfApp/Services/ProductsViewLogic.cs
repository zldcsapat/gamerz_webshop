﻿using AutoMapper;
using BusinessLogic;
using BusinessLogic.Queries;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models.Products;
using WpfApp.ViewModel;
using WpfApp.Models;

namespace WpfApp.Services
{
    class ProductsViewLogic : IProductsViewLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }
        IProductsViewModel ProductVM
        {
            get { return ServiceLocator.Current.GetInstance<IProductsViewModel>(); }
        }
        IEditProductsLogic EditProducts
        {
            get { return ServiceLocator.Current.GetInstance<IEditProductsLogic>(); }
        }
        public void AddCmd()
        {
            Models.ProductsAndPlatformsAndCategoriesVM p = new Models.ProductsAndPlatformsAndCategoriesVM();
            if (EditProducts.EditProduct(p))
            {
                Data.Products product = Mapper.Map<Models.ProductsAndPlatformsAndCategoriesVM, Data.Products>(p);
                BusinessLogic.AddProduct(product);
                ProductVM.Products.Add(p);
            }
        }

        public void DelCmd(Models.ProductsAndPlatformsAndCategoriesVM product)
        {
            if (product == null)
            {
                Messenger.Default.Send(new NotificationMessage("No Product Selected!"));
                return;
            }

            BusinessLogic.DelProduct(product.ID);
            ProductVM.Products.Remove(product);
        }

        public ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM> GetProductList(string txt)
        {
            var productsDB = BusinessLogic.GetProductsAndPlatformsAndCategories(txt).ToList();
            return Mapper.Map<List<GetProductsAndPlatformsAndCategories>, ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM>>(productsDB);
        }

        public void ModCmd(Models.ProductsAndPlatformsAndCategoriesVM product)
        {
            if (product == null)
            {
                Messenger.Default.Send(new NotificationMessage("No Product Selected!"));
                return;
            }

            Models.ProductsAndPlatformsAndCategoriesVM clone = product.Clone();
            if (EditProducts.EditProduct(clone))
            {
                BusinessLogic.ModifyProduct(product.ID, clone.Name, clone.AgeLimit, clone.Description, clone.ReleaseDate, clone.Price, clone.Quantity,clone.CategoryID, clone.PlatformID, clone.Image1);
                product.CopyDataFrom(clone);
            }
        }
    }
}