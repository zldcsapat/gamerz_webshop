﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.Models.JobTypes;
using WpfApp.Views;

namespace WpfApp.Services
{
    class EditUsersLogic : IEditUsersLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        public bool EditUser(Models.UsersAndJobtypes.UsersAndJobTypesVM u)
        {
            EditUsersWindow uw = new EditUsersWindow(u);
            return uw.ShowDialog().Value;
        }

        public ObservableCollection<JobTypesVM> GetJobTypesList()
        {
            var jobtypesDB = BusinessLogic.GetJobTypes().ToList();
            return Mapper.Map<List<Data.JobTypes>, ObservableCollection<JobTypesVM>>(jobtypesDB);
            
        }
    }
}
