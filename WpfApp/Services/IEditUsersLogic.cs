﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;

namespace WpfApp.Services
{
    interface IEditUsersLogic
    {
        bool EditUser(Models.UsersAndJobtypes.UsersAndJobTypesVM u);

        ObservableCollection<Models.JobTypes.JobTypesVM> GetJobTypesList();
    }
}
