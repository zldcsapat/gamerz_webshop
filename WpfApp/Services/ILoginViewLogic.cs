﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Services
{
    interface ILoginViewLogic
    {
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> GetUserList(string txt);
        void LogInCmd(object obj);
    }
}
