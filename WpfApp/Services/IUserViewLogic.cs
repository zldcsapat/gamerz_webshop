﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.Services
{
    interface IUserViewLogic
    {
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> GetUserList(string txt);

        void AddCmd();
        void DelCmd(Models.UsersAndJobtypes.UsersAndJobTypesVM user);
        void ModCmd(Models.UsersAndJobtypes.UsersAndJobTypesVM user);
    }
}
