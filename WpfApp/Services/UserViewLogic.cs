﻿using AutoMapper;
using BusinessLogic;
using BusinessLogic.Queries;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.ViewModel;
using System.Windows.Input;
using System.Windows.Controls;

namespace WpfApp.Services
{
    class UserViewLogic : IUserViewLogic
    {


        IEditUsersLogic EditUsers
        {
            get { return ServiceLocator.Current.GetInstance<IEditUsersLogic>(); }
        }
        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        IUserViewModel UserVM
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewModel>(); }
        }



        public void AddCmd()
        {
            Models.UsersAndJobtypes.UsersAndJobTypesVM u = new Models.UsersAndJobtypes.UsersAndJobTypesVM();
            if (EditUsers.EditUser(u))
            {
                Data.Users user = Mapper.Map<Models.UsersAndJobtypes.UsersAndJobTypesVM, Data.Users>(u);
                BusinessLogic.AddUser(user);
                UserVM.Users = GetUserList("");
                
            }
        }

        public void DelCmd(Models.UsersAndJobtypes.UsersAndJobTypesVM user)
        {
            if (user == null)
            {
                Messenger.Default.Send(new NotificationMessage("No User Selected!"));
                return;
            }

            BusinessLogic.DelUser(user.ID);
            UserVM.Users.Remove(user);
        }

        public void ModCmd(Models.UsersAndJobtypes.UsersAndJobTypesVM user)
        {
            if (user == null)
            {
                Messenger.Default.Send(new NotificationMessage("No User Selected!"));
                return;
            }

            Models.UsersAndJobtypes.UsersAndJobTypesVM clone = user.Clone();
            if (EditUsers.EditUser(clone))
            {
                BusinessLogic.ModifyUser(user.ID, clone.Firstname, clone.Lastname,clone.E_mail,clone.Password,clone.JobTypeID);
                user.CopyDataFrom(clone);
                UserVM.Users = GetUserList("");
            }
        }

        public ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> GetUserList(string txt)
        {
            var usersDB = BusinessLogic.GetUsersJobtypes(txt).ToList();
            return Mapper.Map<List<GetUsersAndJobTypes>, ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM>>(usersDB);
        }
    }
}
