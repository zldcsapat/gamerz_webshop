﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using WpfApp.Models;
using WpfApp.ViewModel;
using WpfApp.Views;

namespace WpfApp.Services
{
    class EditProductsLogic : IEditProductsLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }
        IEditProductsViewModel EditProdVM
        {
            get { return ServiceLocator.Current.GetInstance<IEditProductsViewModel>(); }
        }

        public bool EditProduct(ProductsAndPlatformsAndCategoriesVM p)
        {
            EditProductsWindow pw = new EditProductsWindow(p);
            return pw.ShowDialog().Value;
        }

        public ObservableCollection<CategoriesVM> GetCategoriesList()
        {
            var categoriesDB = BusinessLogic.GetCategroies().ToList();
            return Mapper.Map<List<Data.Categories>, ObservableCollection<CategoriesVM>>(categoriesDB);
        }

        public ObservableCollection<PlatformsVM> GetPlatformsList()
        {
            var platformsDB = BusinessLogic.GetPlatforms().ToList();
            return Mapper.Map<List<Data.Platforms>, ObservableCollection<PlatformsVM>>(platformsDB);
        }

        public void UploadCmd(object img)
        {
            var dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                Filter = "Images (*.jpg,*.png)|*.jpg;*.png|All Files(*.*)|*.*"
            };

            if (dialog.ShowDialog() != true) { return; }

            string file = dialog.FileName;
            BitmapImage image = new BitmapImage(new Uri(file));

            using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                EditProdVM.EditedProduct.Image1 = new byte[fs.Length];
                fs.Read(EditProdVM.EditedProduct.Image1, 0, System.Convert.ToInt32(fs.Length));
            }
            var imageBox = img as Image;
            imageBox.Source = image;
        }
    }
}
