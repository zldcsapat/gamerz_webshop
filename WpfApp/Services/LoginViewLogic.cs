﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.ViewModel;
using WpfApp.Views;
using System.Collections.ObjectModel;
using WpfApp.Models;
using WpfApp.Models.UsersAndJobtypes;
using BusinessLogic.Queries;
using System.Windows.Controls;

namespace WpfApp.Services
{
    class LoginViewLogic : ILoginViewLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        ILoginViewModel LoginVM
        {
            get { return ServiceLocator.Current.GetInstance<ILoginViewModel>(); }
        }

        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        public ObservableCollection<UsersAndJobTypesVM> GetUserList(string txt)
        {
            var usersDB = BusinessLogic.GetUsersJobtypes(txt).ToList();
            return Mapper.Map<List<GetUsersAndJobTypes>, ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM>>(usersDB);
        }

        public void LogInCmd(object obj)
        {
            LoginVM.Users = GetUserList("");
            var passwordBox = obj as PasswordBox;
            var password = passwordBox.Password;
            bool isMatch = true;
            foreach (var item in LoginVM.Users)
            {
                if (item.E_mail == LoginVM.UserEmail && item.Password == password)
                {
                    isMatch = true;
                    MainWindow mw = new MainWindow(item);
                    LoginVM.IsCorrect = false;
                    mw.Show();
                    LoginVM.CloseAction();
                    break; 
                } else
                {
                    isMatch = false;
                }
            }
            if(!isMatch)
            {
                LoginVM.IsCorrect = true;
            }
        }
    }
}
