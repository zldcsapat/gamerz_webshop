﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Services
{
    interface IProductsViewLogic
    {
        ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM> GetProductList(string txt);
        void AddCmd();
        void DelCmd(Models.ProductsAndPlatformsAndCategoriesVM product);
        void ModCmd(Models.ProductsAndPlatformsAndCategoriesVM product);
    }
}
