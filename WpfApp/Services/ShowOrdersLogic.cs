﻿using AutoMapper;
using BusinessLogic;
using BusinessLogic.Queries;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.Views;

namespace WpfApp.Services
{
    class ShowOrdersLogic : IShowOrdersLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        public void CloseCmd(object obj)
        {
            var sw = obj as ShowOrdersWindow;
            sw?.Close();
        }

        public ObservableCollection<OrderProductsVM> GetOrderedProductList(int orderid)
        {
            var orderedpDB = BusinessLogic.GetOrderPorducts(orderid).ToList();
            return Mapper.Map<List<GetOrderProducts>, ObservableCollection<Models.OrderProductsVM>>(orderedpDB);

        }
    }
}
