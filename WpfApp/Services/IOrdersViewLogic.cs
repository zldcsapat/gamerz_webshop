﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Services
{
    interface IOrdersViewLogic
    {
        ObservableCollection<Models.OrderInformationsVM> GetOrderList(string txt,int orderstateid);
        ObservableCollection<Models.OrderStateVM> GetOrderStates();
        void DelCmd(Models.OrderInformationsVM order);
        void ShowCmd(Models.OrderInformationsVM order);
    }
}
