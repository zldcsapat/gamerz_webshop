﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp.ViewModel;

namespace WpfApp.Views
{
    /// <summary>
    /// Interaction logic for EditProductsWindow.xaml
    /// </summary>
    public partial class EditProductsWindow
    {
        public EditProductsWindow(Models.ProductsAndPlatformsAndCategoriesVM p)
        {
            InitializeComponent();
            ServiceLocator.Current.GetInstance<IEditProductsViewModel>().EditedProduct = p;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<bool>(this, ProcessResult);
        }

        private void ProcessResult(bool msg)
        {
            DialogResult = msg;
            Close();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
