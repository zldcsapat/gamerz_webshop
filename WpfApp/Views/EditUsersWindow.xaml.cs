﻿    using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp.ViewModel;

namespace WpfApp.Views
{

    public partial class EditUsersWindow
    {
        public EditUsersWindow(Models.UsersAndJobtypes.UsersAndJobTypesVM u = null)
        {
            InitializeComponent();

            ServiceLocator.Current.GetInstance<IEditUsersViewModel>().EditedUser = u;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<bool>(this, ProcessResult);
        }

        private void ProcessResult(bool msg)
        {
            DialogResult = msg;
            Close();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            ServiceLocator.Current.GetInstance<IEditUsersViewModel>().EditedUser.Password = (sender as PasswordBox).Password;
        }
    }
}
