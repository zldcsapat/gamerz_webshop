﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IUserViewModel
    {
        Models.UsersAndJobtypes.UsersAndJobTypesVM SelectedUser { get; set; }
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> Users { get; set; }
        string SearchText { get; set; }

        ICommand AddCommand { get; }
        ICommand RemoveCommand { get; }
        ICommand ModifyCommand { get; }
    }
}
