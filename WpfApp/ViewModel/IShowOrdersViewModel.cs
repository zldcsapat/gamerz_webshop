﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IShowOrdersViewModel
    {
        Models.OrderInformationsVM CurrentOrder { get; set; }
        ICommand CloseCommand { get; }
        ObservableCollection<Models.OrderProductsVM> OrderedProducts { get; set; }
    }
}
