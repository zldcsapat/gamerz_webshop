﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface ILoginViewModel
    {
        ICommand LogInCommand { get; }
        Action CloseAction { get; set; }
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> Users { get; set; }
        string UserEmail { get; }
        bool IsCorrect { get; set; }
    }
}
