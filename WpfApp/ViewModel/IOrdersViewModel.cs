﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IOrdersViewModel
    {
        Models.OrderInformationsVM SelectedOrder { get; set; }
        ObservableCollection<Models.OrderInformationsVM> Orders { get; set; }
        string SearchText { get; set; }
        int CurrentOrderState { get; set; }

        ObservableCollection<Models.OrderStateVM> OrderStates { get; set; }

        ICommand DetailsCommand { get; }
        ICommand RemoveCommand { get; }
    }
}
