﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IProductsViewModel
    {
        Models.ProductsAndPlatformsAndCategoriesVM SelectedProduct { get; set; }
        ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM> Products { get; set; }
        string SearchText { get; set; }

        ICommand AddCommand { get; }
        ICommand RemoveCommand { get; }
        ICommand ModifyCommand { get; }
    }
}
