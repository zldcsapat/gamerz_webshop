using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using WpfApp.Models;
using WpfApp.Services;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Command;

namespace WpfApp.ViewModel
{

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        Models.UsersAndJobtypes.UsersAndJobTypesVM currentUser;

        public Models.UsersAndJobtypes.UsersAndJobTypesVM CurrentUser { get => currentUser; set => Set(ref currentUser,value); }

        public ICommand LogOutCommand { get; private set; }

        IMainViewLogic MainLogic
        {
            get { return ServiceLocator.Current.GetInstance<IMainViewLogic>(); }
        }

        public MainViewModel()
        {
            LogOutCommand = new RelayCommand<object>(MainLogic.LogOutCmd);
        }
    }
}