﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Models;
using WpfApp.Services;
using WpfApp.Models.UsersAndJobtypes;

namespace WpfApp.ViewModel
{
    public class UserViewModel : ViewModelBase , IUserViewModel
    {
        Models.UsersAndJobtypes.UsersAndJobTypesVM selectedUser;
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> users;
        string searchText;

        public UsersAndJobTypesVM SelectedUser { get => selectedUser; set => Set(ref selectedUser,value); }
        public ObservableCollection<UsersAndJobTypesVM> Users { get => users; set =>Set(ref users,value); }
        public string SearchText { get => searchText; set {
                Set(ref searchText, value);
                Users = UserLogic.GetUserList(searchText);
            } }


        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }
        public ICommand ModifyCommand { get; private set; }

        IUserViewLogic UserLogic
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewLogic>(); }
        }


        public UserViewModel()
        {
            AddCommand = new RelayCommand(UserLogic.AddCmd);
            RemoveCommand = new RelayCommand<Models.UsersAndJobtypes.UsersAndJobTypesVM>(UserLogic.DelCmd);
            ModifyCommand = new RelayCommand<Models.UsersAndJobtypes.UsersAndJobTypesVM>(UserLogic.ModCmd);

            Users = UserLogic.GetUserList("");
        }
    }
}
