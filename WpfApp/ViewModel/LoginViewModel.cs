﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Services;
using WpfApp.Models;
using WpfApp.Models.UsersAndJobtypes;

namespace WpfApp.ViewModel
{
    class LoginViewModel : ViewModelBase, ILoginViewModel
    {
        string userEmail;
        bool isCorrect;
        Action closeAction;
        ObservableCollection<Models.UsersAndJobtypes.UsersAndJobTypesVM> users;

        public string UserEmail { get => userEmail; set => Set(ref userEmail,value); }
        public ObservableCollection<UsersAndJobTypesVM> Users { get => users; set => Set(ref users, value); }
        public Action CloseAction { get => closeAction; set => Set(ref closeAction,value); }
        public bool IsCorrect { get => isCorrect; set => Set(ref isCorrect, value); }


        public ICommand LogInCommand { get; set; }

        ILoginViewLogic LoginLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILoginViewLogic>(); }
        }

        public LoginViewModel()
        {
            LogInCommand = new RelayCommand<object>(LoginLogic.LogInCmd);
            IsCorrect = false;
        }
    }
}
