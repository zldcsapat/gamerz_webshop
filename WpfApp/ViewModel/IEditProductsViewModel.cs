﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IEditProductsViewModel
    {
        Models.ProductsAndPlatformsAndCategoriesVM EditedProduct { get; set; }
        ObservableCollection<Models.PlatformsVM> Platforms { get; set; }
        ObservableCollection<Models.CategoriesVM> Categories { get; set; }

        ICommand OKCommand { get; }
        ICommand CancelCommand { get; }
        ICommand UploadCommand { get; }
     }
}
