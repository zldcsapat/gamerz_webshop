﻿ using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IEditUsersViewModel
    {
        Models.UsersAndJobtypes.UsersAndJobTypesVM EditedUser { get; set; }
        ObservableCollection<Models.JobTypes.JobTypesVM> JobTypes { get; set; }

        ICommand OKCommand { get; }
        ICommand CancelCommand { get; }
    }
}
