﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Models;
using WpfApp.Models.UsersAndJobtypes;
using System.Collections.ObjectModel;
using WpfApp.Models.JobTypes;
using WpfApp.Services;
using Microsoft.Practices.ServiceLocation;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace WpfApp.ViewModel
{
    public class EditUsersViewModel : ViewModelBase, IEditUsersViewModel
    {

        Models.UsersAndJobtypes.UsersAndJobTypesVM editedUser;

        ObservableCollection<JobTypesVM> jobTypes;

        public UsersAndJobTypesVM EditedUser { get => editedUser; set => Set(ref editedUser,value); }
        public ObservableCollection<JobTypesVM> JobTypes { get => jobTypes; set => Set(ref jobTypes,value); }
    
        public ICommand CancelCommand { get; set; }
        public ICommand OKCommand { get; set; }

        IEditUsersLogic EditUsersLogic
        {
            get { return ServiceLocator.Current.GetInstance<IEditUsersLogic>(); }
        }

        public EditUsersViewModel()
        {
            CancelCommand = new RelayCommand(() => Messenger.Default.Send(false));
            OKCommand = new RelayCommand(() => Messenger.Default.Send(true));

            JobTypes = EditUsersLogic.GetJobTypesList();
        }
    }
}
