﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Models;
using WpfApp.Services;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Command;

namespace WpfApp.ViewModel
{
    public class OrdersViewModel : ViewModelBase, IOrdersViewModel
    {
        Models.OrderInformationsVM selectedOrder;
        ObservableCollection<Models.OrderInformationsVM> orders;
        string searchText = "";
        ObservableCollection<Models.OrderStateVM> orderStates;
        int currentOrderState = 1;

        public int CurrentOrderState { get => currentOrderState; set
            {
                Set(ref currentOrderState, value);
                Orders = OrderLogic.GetOrderList(searchText, currentOrderState);
            }
        }
        public OrderInformationsVM SelectedOrder { get => selectedOrder; set => Set(ref selectedOrder,value); }
        public ObservableCollection<OrderInformationsVM> Orders { get => orders; set => Set(ref orders,value); }
        public ObservableCollection<OrderStateVM> OrderStates { get => orderStates; set => Set(ref orderStates,value); }
        public string SearchText
        {
            get => searchText; set
            {
                Set(ref searchText, value);
                Orders = OrderLogic.GetOrderList(searchText,currentOrderState);
            }
        }

        IOrdersViewLogic OrderLogic
        {
            get { return ServiceLocator.Current.GetInstance<IOrdersViewLogic>(); }
        }

        public ICommand DetailsCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        public OrdersViewModel()
        {
            DetailsCommand = new RelayCommand<Models.OrderInformationsVM>(OrderLogic.ShowCmd);
            RemoveCommand = new RelayCommand<Models.OrderInformationsVM>(OrderLogic.DelCmd);
            orderStates = OrderLogic.GetOrderStates();
            Orders = OrderLogic.GetOrderList(SearchText,CurrentOrderState);

        }
    }
}
