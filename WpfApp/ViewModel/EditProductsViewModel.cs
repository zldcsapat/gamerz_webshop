﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Models;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
    public class EditProductsViewModel : ViewModelBase, IEditProductsViewModel
    {
        Models.ProductsAndPlatformsAndCategoriesVM editedProduct;
        ObservableCollection<Models.PlatformsVM> platforms;
        ObservableCollection<Models.CategoriesVM> categories;

        public ProductsAndPlatformsAndCategoriesVM EditedProduct { get => editedProduct; set => Set(ref editedProduct,value); }
        public ObservableCollection<PlatformsVM> Platforms { get => platforms; set => Set(ref platforms,value); }
        public ObservableCollection<CategoriesVM> Categories { get => categories; set => Set(ref categories,value); }

        public ICommand OKCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand UploadCommand { get; set; }

        IEditProductsLogic EditProductsLogic
        {
            get { return ServiceLocator.Current.GetInstance<IEditProductsLogic>(); }
        }

        public EditProductsViewModel()
        {
            OKCommand = new RelayCommand(() => Messenger.Default.Send(true));
            CancelCommand = new RelayCommand(() => Messenger.Default.Send(false));
            UploadCommand = new RelayCommand<object>(EditProductsLogic.UploadCmd);

            Platforms = EditProductsLogic.GetPlatformsList();
            Categories = EditProductsLogic.GetCategoriesList();
        }
    }
}
