/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:WpfApp"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace WpfApp.ViewModel
{

    public class ViewModelLocator
    {

        public ViewModelLocator()
        {
            //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);



            //SimpleIoc.Default.Register<MainViewModel>();
        }

        public IEditUsersViewModel EditUsersVM
        {
            get { return ServiceLocator.Current.GetInstance<IEditUsersViewModel>(); }
        }

        public IEditProductsViewModel EditProductsVM
        {
            get { return ServiceLocator.Current.GetInstance<IEditProductsViewModel>(); }
        }
        
        public IUserViewModel UsersVM
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewModel>(); }
        }

        public IMainViewModel MainVM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IMainViewModel>();
            }
        }
        
        public IProductsViewModel ProductVM
        {
            get { return ServiceLocator.Current.GetInstance<IProductsViewModel>(); }
        }

        public ILoginViewModel LoginVM
        {
            get { return ServiceLocator.Current.GetInstance<ILoginViewModel>(); }
        }

        public IOrdersViewModel OrderVM
        {
            get { return ServiceLocator.Current.GetInstance<IOrdersViewModel>(); }
        }

        public IShowOrdersViewModel ShowOrdersVM
        {
            get { return ServiceLocator.Current.GetInstance<IShowOrdersViewModel>(); }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}