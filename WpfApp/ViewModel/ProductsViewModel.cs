﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Models.UsersAndJobtypes;
using WpfApp.Models.Products;
using WpfApp.Services;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Command;

namespace WpfApp.ViewModel
{
    public class ProductsViewModel : ViewModelBase, IProductsViewModel
    {
        Models.ProductsAndPlatformsAndCategoriesVM selectedProduct;
        ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM> products;
        string searchText;

        public Models.ProductsAndPlatformsAndCategoriesVM SelectedProduct { get => selectedProduct; set => Set(ref selectedProduct, value); }
        public ObservableCollection<Models.ProductsAndPlatformsAndCategoriesVM> Products { get => products; set => Set(ref products, value); }
        public string SearchText { get => searchText; set {
                Set(ref searchText, value);
                Products = ProductLogic.GetProductList(searchText);
            } }
        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }
        public ICommand ModifyCommand { get; private set; }

        IProductsViewLogic ProductLogic
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IProductsViewLogic>();
            }
        }

        public ProductsViewModel()
        {
            AddCommand = new RelayCommand(ProductLogic.AddCmd);
            RemoveCommand = new RelayCommand<Models.ProductsAndPlatformsAndCategoriesVM>(ProductLogic.DelCmd);
            ModifyCommand = new RelayCommand<Models.ProductsAndPlatformsAndCategoriesVM>(ProductLogic.ModCmd);
            Products = ProductLogic.GetProductList("");
        }
    }
}
