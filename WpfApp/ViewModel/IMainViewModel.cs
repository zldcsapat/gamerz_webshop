﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.ViewModel
{
    public interface IMainViewModel
    {
        Models.UsersAndJobtypes.UsersAndJobTypesVM CurrentUser { get; set; }
        ICommand LogOutCommand { get; }
    }
}
