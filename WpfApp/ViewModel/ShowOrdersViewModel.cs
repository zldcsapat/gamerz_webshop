﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Models;
using WpfApp.Services;
using System.Collections.ObjectModel;

namespace WpfApp.ViewModel
{
    public class ShowOrdersViewModel : ViewModelBase, IShowOrdersViewModel
    {
        Models.OrderInformationsVM currentOrder;
        ObservableCollection<Models.OrderProductsVM> orderedProducts;

        public OrderInformationsVM CurrentOrder { get => currentOrder;
            set
            {
                Set(ref currentOrder, value);
                OrderedProducts = ShowOrderLogic.GetOrderedProductList(currentOrder.OrderID);
            }
        }
        public ObservableCollection<OrderProductsVM> OrderedProducts { get => orderedProducts; set => Set(ref orderedProducts,value); }

        public ICommand CloseCommand { get; private set; }

        IShowOrdersLogic ShowOrderLogic
        {
            get { return ServiceLocator.Current.GetInstance<IShowOrdersLogic>(); }
        }


        public ShowOrdersViewModel()
        {
            CloseCommand = new RelayCommand<object>(ShowOrderLogic.CloseCmd);
        }
    }
}
