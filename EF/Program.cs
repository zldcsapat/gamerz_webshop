﻿using System;
using Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using System.Security.Cryptography;
using HashLib;
using SHA3;

namespace EF
{
    class Program
    {
        static void Main(string[] args)
        {
            string Password = "";

            //IHash hash = HashFactory.Crypto.SHA3.CreateKeccak512();
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //SHA3.SHA3Managed hash2 = null;

            var hash2 = new SHA3Unmanaged(512);
            byte[] bytes = encoding.GetBytes(Password);
            byte[] r1 = hash2.ComputeHash(bytes);
            var hash3 = BitConverter.ToString(r1);
            //var hash4 = ByteToString(r1);


            HashAlgorithm hashAlgo = HashFactory.Wrappers.HashToHashAlgorithm(hash);
            HashResult r = hash.ComputeString(Password, System.Text.Encoding.ASCII);

            //SHA3 - 512("")

            //Keccak - 512('abc');
            //Keccak[1024]('abc', 512);

            //SHA3 - 512('abc');
            //Keccak[1024]('abc' || 01, 512);

            Password = r.ToString();
            Console.WriteLine(r.ToString().ToLower().Replace("-", ""));
            Console.WriteLine("{0}, {1}, {2}", hash.BlockSize, hash.HashSize, hash.Name);
            Console.ReadLine();
        }
    }
}
