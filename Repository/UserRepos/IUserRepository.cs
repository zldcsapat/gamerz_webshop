﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UserRepos
{
    public interface IUserRepository : IRepository<Users>
    {
        void Modify(int id, string fname, string lname, string email, string pass, int jobtype);
    }
}
