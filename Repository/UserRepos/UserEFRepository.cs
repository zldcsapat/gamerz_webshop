﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.UserRepos
{
    public class UserEFRepository : EFRepository<Users>, IUserRepository
    {
        public UserEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override Users GetById(int id)
        {
            return Get(user => user.ID == id).SingleOrDefault();
        }

        public void Modify(int id, string fname, string lname, string email, string pass, int jobtype)
        {
            Users user = GetById(id);
            if (user == null) throw new ArgumentException("NO DATA");
            if (fname != null) user.FirstName = fname;
            if (lname != null) user.LastName = lname;
            if (email != null) user.E_mail = email;
            if (pass != null) user.Password = pass;
            user.JobTypeID = jobtype;
            context.SaveChanges(); //TODO:ConcurrencyException ??? (le kell kezelni)

        }
    }
}
