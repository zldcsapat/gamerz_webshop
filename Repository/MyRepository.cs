﻿using Repository;
using Repository.CategoryRepos;
using Repository.CustomerRepos;
using Repository.JobTypesRepos;
using Repository.OrderProductsRepos;
using Repository.OrderRepos;
using Repository.OrderStateRepos;
using Repository.PlatformRepos;
using Repository.ProductRepos;
using Repository.UserRepos;
using Repository.WishListRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class MyRepository
    {
        public ICategoryRepository CategoryRepository { get; private set; }
        public ICustomerRepository CustomerRepository { get; private set; }
        public IJobyTypesRepository JobtypeRepository { get; private set; }
        public IOrderRepository OrderRepository { get; private set; }
        public IPlatformRepository PlatformRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }
        public IUserRepository UserRepository { get; private set; }
        public IWishListRepository WishlistRepository { get; private set; }
        public IOrderProductsRepository OrderProductsRepository { get; set; }
        public IOrderStateRepository OrderStateRepository { get; set; }

        public MyRepository (ICategoryRepository catr, ICustomerRepository cusr, IJobyTypesRepository jobtyper, IOrderRepository orderr, IPlatformRepository platformr, IProductRepository productr, IUserRepository userr, IWishListRepository wishlistr, IOrderProductsRepository orderproductsr , IOrderStateRepository orderstater)
        {
            CategoryRepository = catr;
            CustomerRepository = cusr;
            JobtypeRepository = jobtyper;
            OrderRepository = orderr;
            PlatformRepository = platformr;
            ProductRepository = productr;
            UserRepository = userr;
            WishlistRepository = wishlistr;
            OrderProductsRepository = orderproductsr;
            OrderStateRepository = orderstater;
        }
    }
}
