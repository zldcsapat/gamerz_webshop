﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.OrderProductsRepos
{
    public class OrderProductsEFRepository : EFRepository<OrderProducts>, IOrderProductsRepository
    {
        public OrderProductsEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override OrderProducts GetById(int id)
        {
            return Get(orderp => orderp.ID == id).SingleOrDefault();
        }
    }
}
