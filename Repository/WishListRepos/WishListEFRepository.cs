﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.WishListRepos
{
    public class WishListEFRepository : EFRepository<WishList>, IWishListRepository
    {
        public WishListEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override WishList GetById(int id)
        {
            return Get(wishlist => wishlist.ID == id).SingleOrDefault();
        }
    }
}
