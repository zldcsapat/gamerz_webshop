﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.OrderRepos
{
    public interface IOrderRepository : IRepository<Orders>
    {
        void Modify(int id, int orderstateid);
    }
}
