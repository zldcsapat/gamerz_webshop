﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.OrderRepos
{
    public class OrderEFRepository : EFRepository<Orders>, IOrderRepository
    {
        public OrderEFRepository(DbContext newctx) : base(newctx)
        {
        }
        public override Orders GetById(int id)
        {
            return Get(order => order.ID == id).SingleOrDefault();
        }

        public void Modify(int id, int orderstateid)
        {
            Orders order = GetById(id);
            order.OrderStateID = orderstateid;
            context.SaveChanges();
        }
    }
}
