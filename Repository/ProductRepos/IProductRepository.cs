﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ProductRepos
{
    public interface IProductRepository : IRepository<Products>
    {
        void Modify(int id, string name, int agelimit, string description, DateTime releasedate, int price, int quantity, int categoryid, int platformid, byte[] image1);
    }
}
