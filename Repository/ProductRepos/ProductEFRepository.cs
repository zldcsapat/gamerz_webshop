﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.ProductRepos
{
    public class ProductEFRepository : EFRepository<Products>, IProductRepository
    {
        public ProductEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override Products GetById(int id)
        {
            return Get(product => product.ID == id).SingleOrDefault();
        }


        public void Modify(int id, string name, int agelimit, string description, DateTime releasedate, int price, int quantity, int categoryid, int platformid, byte[] image1)
        {
            Products product = GetById(id);
            if (product == null) throw new ArgumentException("NO DATA");
            if (name != null) product.Name = name;
            if (description != null) product.Description = description;
            product.AgeLimit = agelimit;
            if(releasedate != null) product.ReleaseDate = releasedate;
            product.CategoryID = categoryid;
            product.PlatformID = platformid;
            product.image1 = image1;
            product.Price = price;
            product.Quantity = quantity;
            context.SaveChanges();
        }
    }
}
