﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.PlatformRepos
{
    public class PlatformEFRepository : EFRepository<Platforms>, IPlatformRepository
    {
        public PlatformEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override Platforms GetById(int id)
        {
            return Get(platform => platform.ID == id).SingleOrDefault();
        }
    }
}
