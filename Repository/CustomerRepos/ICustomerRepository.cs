﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.CustomerRepos
{
    public interface ICustomerRepository : IRepository<Customers>
    {
        void Modify(int id, string fname, string lname, string confirmpassword, string pass, string phone, int zip, string town, string street, int streetno);
    }

}
