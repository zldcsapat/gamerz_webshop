﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.CustomerRepos
{
    public class CustomerEFRepository : EFRepository<Customers>, ICustomerRepository 
    {
        public CustomerEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override Customers GetById(int id)
        {
            return Get(customer => customer.ID == id).SingleOrDefault();
        }

        

        public void Modify(int id, string fname, string lname, string confirmpass, string pass, string phone, int zip, string town, string street, int streetno)
        {
            Customers customer = GetById(id);
            if (customer == null) throw new ArgumentException("NO DATA");
            if (fname != null) customer.FirstName = fname;
            if (lname != null) customer.LastName = lname;
            if (confirmpass != null) customer.ConfirmPassword = confirmpass;
            if (pass != null) customer.Password = pass;
            if (phone != null) customer.PhoneNumber = phone;
            customer.ZipCode = zip;
            if (town != null) customer.Town = town;
            if (street != null) customer.Street = street;
            customer.StreetNumber = streetno;
            context.SaveChanges(); //TODO:ConcurrencyException ??? (le kell kezelni)
        }
    }
}
