﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.JobTypesRepos
{
    public class JobTypesEFRepository : EFRepository<JobTypes>, IJobyTypesRepository
    {
        public JobTypesEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override JobTypes GetById(int id)
        {
            return Get(jobtype => jobtype.ID == id).SingleOrDefault();
        }
    }
}
