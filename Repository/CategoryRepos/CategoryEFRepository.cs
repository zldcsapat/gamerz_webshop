﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.CategoryRepos
{
    public class CategoryEFRepository : EFRepository<Categories>, ICategoryRepository
    {
        public CategoryEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override Categories GetById(int id)
        {
            return Get(category => category.ID == id).SingleOrDefault();
        }
    }
}
