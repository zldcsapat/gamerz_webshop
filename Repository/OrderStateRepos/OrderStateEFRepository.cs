﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.OrderStateRepos
{
    public class OrderStateEFRepository : EFRepository<OrderState>, IOrderStateRepository
    {
        public OrderStateEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override OrderState GetById(int id)
        {
            return Get(orders => orders.ID == id).SingleOrDefault();
        }
    }
}
