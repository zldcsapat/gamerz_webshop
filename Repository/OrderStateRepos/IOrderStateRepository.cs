﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.OrderStateRepos
{
    public interface IOrderStateRepository : IRepository<OrderState>
    {
    }
}
