﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Queries;
using Data;

namespace Gamerz.Test
{
    class FakeLogic : ILogic
    {
        public void AddCustomer(Customers newcustomer)
        {
            throw new NotImplementedException();
        }

        public void AddOrderProducts(OrderProducts neworderproducts)
        {
            throw new NotImplementedException();
        }

        public void AddOrders(Orders neworders)
        {
            throw new NotImplementedException();
        }

        public void AddProduct(Products newproduct)
        {
            throw new NotImplementedException();
        }

        public void AddUser(Users newuser)
        {
            throw new NotImplementedException();
        }

        public void AddWishList(WishList newwishlist)
        {
            throw new NotImplementedException();
        }

        public void DelProduct(int id)
        {
            throw new NotImplementedException();
        }

        public void DelUser(int id)
        {
            throw new NotImplementedException();
        }

        public void DelWishListItem(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetEverythingAboutOrders> EverythingOrder(string search, int orderstateid)
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetEverythingAboutOrders> EverythingOrder()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Categories> GetCategroies()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Customers> GetCustomers()
        {
            throw new NotImplementedException();
        }

        public IQueryable<JobTypes> GetJobTypes()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Products> GetNewestProducts()
        {
            throw new NotImplementedException();
        }

        public Customers GetOneCustomer(int id)
        {
            throw new NotImplementedException();
        }

        public Products GetOneProduct(int id)
        {
            throw new NotImplementedException();
        }

        public Users GetOneUser(int id)
        {
            throw new NotImplementedException();
        }

        public WishList GetOneWishList(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrderProducts> GetOrderProducts()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Orders> GetOrders()
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrderState> GetOrderStates()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Platforms> GetPlatforms()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Products> GetProducts()
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetProductsAndPlatformsAndCategories> GetProductsAndPlatformsAndCategories(string search)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Products> GetThreeGames()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Products> GetTwoGames()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Users> GetUsers()
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetUsersAndJobTypes> GetUsersJobtypes(string search)
        {
            throw new NotImplementedException();
        }

        public IQueryable<WishList> GetWishList()
        {
            throw new NotImplementedException();
        }

        public void ModifyCustomer(int id, string firstname, string lastname, string password, string confirmpassword, string phonenumber, int zipcode, string town, string street, int streetnumber)
        {
            throw new NotImplementedException();
        }

        public void ModifyOrder(int id, int orderstateid)
        {
            throw new NotImplementedException();
        }

        public void ModifyProduct(int id, string name, int agelimit, string description, DateTime releasedate, int price, int quantity, int categoryid, int platformid, byte[] image1)
        {
            throw new NotImplementedException();
        }

        public void ModifyUser(int id, string fname, string lname, string email, string pass, int jobtype)
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetOrdersAndCustomersAndProducts> Order()
        {
            throw new NotImplementedException();
        }

        public IQueryable<ProductsHighPrice> ProductsHighPrice()
        {
            throw new NotImplementedException();
        }

        public IQueryable<ProductsInPlatform> ProductsInPlatform(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ProductsLowPrice> ProductsLowPrice()
        {
            throw new NotImplementedException();
        }

        public IQueryable<GetWishlistGame> WishList()
        {
            throw new NotImplementedException();
        }
    }
}
