﻿using BusinessLogic.Queries;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    
    public interface ILogic
    {   
        #region teljes tabla lekeresek
        //teljes tábla lekérések
        IQueryable<Categories> GetCategroies();
        IQueryable<Customers> GetCustomers();
        IQueryable<JobTypes> GetJobTypes();
        IQueryable<Orders> GetOrders();
        IQueryable<OrderProducts> GetOrderProducts();
        IQueryable<OrderState> GetOrderStates();
        IQueryable<Platforms> GetPlatforms();
        IQueryable<Products> GetProducts();
        IQueryable<Users> GetUsers();
        IQueryable<WishList> GetWishList();
        #endregion

        #region User lekeres, hozzaadas, torles, modositas
        //User lekérés, hozzáadás, törlés, módosítás
        Users GetOneUser(int id);
        void AddUser(Users newuser);
        void DelUser(int id);
        void ModifyUser(int id, string fname, string lname, string email, string pass, int jobtype);
        #endregion

        #region Customer modosita, lekeres, hozzaadas, id
        //Customer módosítás, lekérés,hozzáadás, id
        void ModifyCustomer(int id, string firstname, string lastname, string password, string confirmpassword, string phonenumber, int zipcode, string town, string street, int streetnumber);
        Customers GetOneCustomer(int id);
        void AddCustomer(Customers newcustomer);
        #endregion

        #region dolgozok es munkakoruk
        //dolgozók és a munkakörük
        IQueryable<GetUsersAndJobTypes> GetUsersJobtypes(string search);

        #endregion

        #region Product lekeres, hozzaadas, torles, modositas
        //Product lekérés, hozzáadás, törlés, módosítás
        Products GetOneProduct(int id);
        IQueryable<ProductsInPlatform> ProductsInPlatform(int id);
        IQueryable<ProductsLowPrice> ProductsLowPrice();
        IQueryable<ProductsHighPrice> ProductsHighPrice();
        void AddProduct(Products newproduct);
        void DelProduct(int id);
        void ModifyProduct(int id, string name, int agelimit, string description, DateTime releasedate, int price, int quantity, int categoryid, int platformid, byte[] image1);
        #endregion

        #region termekek+platformok+katego
        // Termékek+platformok+katego
        IQueryable<GetProductsAndPlatformsAndCategories> GetProductsAndPlatformsAndCategories(string search);
        IQueryable<GetOrdersAndCustomersAndProducts> Order();
        #endregion

        #region Wishlist lekeres, hozzadas, torles
        //WishList lekérés, hozzáadás, törlés
        WishList GetOneWishList(int id);
        void AddWishList(WishList newwishlist);
        void DelWishListItem(int id);
        //Kívánságlista
        IQueryable<GetWishlistGame> WishList();
        #endregion

        #region minden ami order
        //Minden, ami order
        IQueryable<GetOrderInformations> GetOrderInformations(string search,int orderstateid);
        IQueryable<GetOrderProducts> GetOrderPorducts(int orderid);
        void ModifyOrder(int id, int orderstateid);
        void AddOrders(Orders neworders);
        void AddOrderProducts(OrderProducts neworderproducts);
        int GetNextOrderId();
        #endregion

        #region legujabb jatekok
        IQueryable<ProductsLowPrice> GetNewestProducts();
        #endregion
        
        #region 2 jatek
        IQueryable<ProductsLowPrice> GetTwoGames();
        #endregion

        #region 3 jatek
        IQueryable<ProductsLowPrice> GetThreeGames();
        #endregion
    }
}
