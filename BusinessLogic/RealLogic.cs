﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;
using Repository;
using Repository.CategoryRepos;
using Repository.CustomerRepos;
using Repository.JobTypesRepos;
using Repository.OrderRepos;
using Repository.PlatformRepos;
using Repository.ProductRepos;
using Repository.UserRepos;
using Repository.WishListRepos;
using BusinessLogic.Queries;
using Repository.OrderProductsRepos;
using Repository.OrderStateRepos;

namespace BusinessLogic
{
    public class RealLogic : ILogic
    {

        #region Repo
        MyRepository Repo;

        public RealLogic(MyRepository newRepo)
        {
            Repo = newRepo;
        }
        #endregion

        #region osszes elem lehivasa
        //összes elem lehívása
        public IQueryable<Categories> GetCategroies()
        {
            return Repo.CategoryRepository.GetAll();
        }

        public IQueryable<Customers> GetCustomers()
        {
            return Repo.CustomerRepository.GetAll();
        }

        public IQueryable<JobTypes> GetJobTypes()
        {
            return Repo.JobtypeRepository.GetAll();
        }

        public IQueryable<Orders> GetOrders()
        {
            return Repo.OrderRepository.GetAll();
        }

        public IQueryable<Platforms> GetPlatforms()
        {
            return Repo.PlatformRepository.GetAll();
        }

        public IQueryable<Products> GetProducts()
        {
            return Repo.ProductRepository.GetAll();
        }

        public IQueryable<Users> GetUsers()
        {
            return Repo.UserRepository.GetAll();
        }

        public IQueryable<WishList> GetWishList()
        {
            return Repo.WishlistRepository.GetAll();
        }

        public IQueryable<OrderProducts> GetOrderProducts()
        {
            return Repo.OrderProductsRepository.GetAll();
        }

        public IQueryable<OrderState> GetOrderStates()
        {
            return Repo.OrderStateRepository.GetAll();
        }
        #endregion

        #region public RealLogic
        public RealLogic()
        {
            gamerzDBEntities WE = new gamerzDBEntities();

            CategoryEFRepository catr = new CategoryEFRepository(WE);
            CustomerEFRepository cusr = new CustomerEFRepository(WE);
            JobTypesEFRepository jobtyper = new JobTypesEFRepository(WE);
            OrderEFRepository orderr = new OrderEFRepository(WE);
            PlatformEFRepository platformr = new PlatformEFRepository(WE);
            ProductEFRepository productr = new ProductEFRepository(WE);
            UserEFRepository userr = new UserEFRepository(WE);
            WishListEFRepository wishlistr = new WishListEFRepository(WE);
            OrderProductsEFRepository orderproductsr = new OrderProductsEFRepository(WE);
            OrderStateEFRepository orderstater = new OrderStateEFRepository(WE);
            Repo = new MyRepository(catr, cusr, jobtyper, orderr, platformr, productr, userr, wishlistr, orderproductsr,orderstater);
        }
        #endregion

        #region Wishlist hozzaadas, torles, getbyid
        // WishList hozzáadás,törlés, getbyid

        public void AddWishList(WishList newwishlist)
        {
            Repo.WishlistRepository.Insert(newwishlist);
        }

        public void DelWishListItem(int id)
        {
            Repo.WishlistRepository.Delete(id);
        }

        public WishList GetOneWishList(int id)
        {
            return Repo.WishlistRepository.GetById(id);
        }
        #endregion

        #region User lekeres, hozzaadas, torles, modositas
        //User lekérés, hozzáad, törlés, módosítás
        public Users GetOneUser(int id)
        {
            return Repo.UserRepository.GetById(id);
        }

        public void AddUser(Users newuser)
        {
            Repo.UserRepository.Insert(newuser);
        }

        public void DelUser(int id)
        {
            Repo.UserRepository.Delete(id);
        }

        public void ModifyUser(int id, string fname, string lname, string email, string pass, int jobtype)
        {
            Repo.UserRepository.Modify(id, fname, lname, email, pass, jobtype);
        }
        #endregion

        #region Product lekeres, hozzaadas, torles, modositas
        //Product lekérés, hozzáadás, törlés, módosítás        
        public Products GetOneProduct(int id)
        {
            return Repo.ProductRepository.GetById(id);
        }

        public void AddProduct(Products newproduct)
        {
            Repo.ProductRepository.Insert(newproduct);
        }

        public void DelProduct(int id)
        {
            Repo.ProductRepository.Delete(id);
        }

        
        public void ModifyProduct(int id, string name, int agelimit, string description, DateTime releasedate, int price, int quantity, int categoryid, int platformid, byte[] image1)
        {
            Repo.ProductRepository.Modify(id,name,agelimit,description,releasedate,price,quantity,categoryid,platformid,image1);
        }
        #endregion

        #region Customer id, lekeres, modositas
        //Customer id, lekérés, módosítás
        public Customers GetOneCustomer(int id)
        {
            return Repo.CustomerRepository.GetById(id);
        }

        public void ModifyCustomer(int id, string firstname, string lastname, string password, string confirmpassword, string phonenumber, int zipcode, string town, string street, int streetnumber)
        {
            Repo.CustomerRepository.Modify(id, firstname, lastname, password, confirmpassword, phonenumber, zipcode, town, street, streetnumber);
        }

        public void AddCustomer(Customers newcustomer)
        {
            Repo.CustomerRepository.Insert(newcustomer);
        }
        #endregion

        #region ketgoria lekeres
        //kategória lekérés
        public Categories GetOneCategory(int id)
        {
            return Repo.CategoryRepository.GetById(id);
        }
        #endregion

        #region Felhasznalok es munkakoruk
        //Felhasználók és munkakörök
        public IQueryable<GetUsersAndJobTypes> GetUsersJobtypes(string search)
        {
            var result = from user in Repo.UserRepository.GetAll()
                        join job in Repo.JobtypeRepository.GetAll() on user.JobTypeID equals job.ID
                        where   user.LastName.Contains(search) || user.FirstName.Contains(search) ||
                                user.E_mail.Contains(search) || job.Name.Contains(search)
                        select new GetUsersAndJobTypes {
                            ID = user.ID,
                            Firstname = user.FirstName,
                            Lastname = user.LastName,
                            E_mail = user.E_mail,
                            Password = user.Password,
                            JobTypeID = user.JobTypeID,
                            JobTypeName = job.Name
                        };
            return result;
        }
        #endregion

        #region Kivansaglista
        //Kívánsláglista
        public IQueryable<GetWishlistGame> WishList()
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         join wishlist in Repo.WishlistRepository.GetAll() on products.ID equals wishlist.ProductID
                         select new GetWishlistGame
                         {
                            WishlistID = wishlist.ID,
                            GameID = (int)wishlist.ProductID,
                            CustomerID = (int)wishlist.CustomerID,
                            GameName = products.Name,
                            GamePrice = products.Price,
                            Description = products.Description,
                            Image1 = products.image1
                         };
            return result;
                         
        }
        #endregion

        #region Jatek kategoriak es platformok
        public IQueryable<GetProductsAndPlatformsAndCategories> GetProductsAndPlatformsAndCategories(string search)
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         join platforms in Repo.PlatformRepository.GetAll() on products.PlatformID equals platforms.ID
                         join categories in Repo.CategoryRepository.GetAll() on products.CategoryID equals categories.ID
                         where products.Name.Contains(search) || platforms.Name.Contains(search) || categories.Name.Contains(search)
                         select new GetProductsAndPlatformsAndCategories
                         {
                             ID = products.ID,
                             Name = products.Name,
                             Quantity = products.Quantity,
                             Price = products.Price,
                             AgeLimit = products.AgeLimit,
                             ReleaseDate = products.ReleaseDate,
                             Description = products.Description,
                             image1 = products.image1,
                             CategoryName = categories.Name,
                             CategoryID = categories.ID,
                             PlatformID = platforms.ID,
                             PlatformName = platforms.Name
                         };
            return result;
        }
        #endregion

        #region Vasarlok rendelesei
        public IQueryable<GetOrdersAndCustomersAndProducts> Order()
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         join orderproducts in Repo.OrderProductsRepository.GetAll() on products.ID equals orderproducts.ProductID
                         join orders in Repo.OrderRepository.GetAll() on orderproducts.OrderID equals orders.ID
                         join customers in Repo.CustomerRepository.GetAll() on orders.CustomerID equals customers.ID
                         select new GetOrdersAndCustomersAndProducts
                         {
                             OrderID = orders.ID,
                             CustomerID = customers.ID,
                             ProductID = products.ID,
                             ProductName = products.Name,
                             Description = products.Description,
                             Price = products.Price,
                             image1 = products.image1,
                             SubTotal = products.Price * orderproducts.Quantity
                         };
            return result;
        }
        #endregion

        #region Jatekok platform szerint
        public IQueryable<ProductsInPlatform> ProductsInPlatform(int id)
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         join platform in Repo.PlatformRepository.GetAll() on products.PlatformID equals platform.ID
                         where products.PlatformID.Equals(id)
                         select new ProductsInPlatform
                         {
                             ID = products.ID,
                             PlatformID = platform.ID,
                             Name = products.Name,
                             Price = products.Price,
                             image1 = products.image1
                         };
                         return result;
        }
        #endregion

        #region Termekek ar szerinti rendezese
        public IQueryable<ProductsLowPrice> ProductsLowPrice()
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         orderby products.Price ascending
                         select new ProductsLowPrice
                         {
                             ID = products.ID,
                             Name = products.Name,
                             Price = products.Price,
                             image1 = products.image1
                         };
            return result;
        }

        public IQueryable<ProductsHighPrice> ProductsHighPrice()
        {
            var result = from products in Repo.ProductRepository.GetAll()
                         orderby products.Price descending
                         select new ProductsHighPrice
                         {
                             ID = products.ID,
                             Name = products.Name,
                             Price = products.Price,
                             image1 = products.image1
                         };
            return result;
        }
        #endregion

        #region Minden ami rendeles
        public IQueryable<GetOrderInformations> GetOrderInformations(string search, int orderstateid)
        {
            var result = from orders in Repo.OrderRepository.GetAll()
                         join orderstate in Repo.OrderStateRepository.GetAll() on orders.OrderStateID equals orderstate.ID
                         join customers in Repo.CustomerRepository.GetAll() on orders.CustomerID equals customers.ID
                         where orders.OrderStateID == orderstateid && 
                         (customers.FirstName.Contains(search) || customers.LastName.Contains(search) ||
                         customers.Town.Contains(search) || customers.Street.Contains(search))
                         select new GetOrderInformations
                         {
                             OrderID = orders.ID,
                             OrderStateID = orders.OrderStateID,
                             OrderStateName = orderstate.Name,
                             CusFirstName = customers.FirstName,
                             CusLastName = customers.LastName,
                             CusEmail = customers.E_mail,
                             CusPhone = customers.PhoneNumber,
                             CusZipCode = customers.ZipCode,
                             CusTown = customers.Town,
                             CusStreet = customers.Street,
                             CusStreetNumber = customers.StreetNumber
                         };
            return result;
                      
        }

        public IQueryable<GetOrderProducts> GetOrderPorducts(int orderid)
        {
            var result = from orderproducts in Repo.OrderProductsRepository.GetAll()
                         join products in Repo.ProductRepository.GetAll() on orderproducts.ProductID equals products.ID
                         join platforms in Repo.PlatformRepository.GetAll() on products.PlatformID equals platforms.ID
                         where orderproducts.OrderID == orderid
                         select new GetOrderProducts
                         {
                             ID = orderproducts.ID,
                             ProductName = products.Name,
                             ProductImage = products.image1,
                             ProductPrice = products.Price,
                             ProductQuantity = orderproducts.Quantity,
                             ProductReleaseDate = products.ReleaseDate,
                             PlatformName = platforms.Name
                         };
            return result;
        }

        public void AddOrders(Orders neworders)
        {
            Repo.OrderRepository.Insert(neworders);
        }

        public void AddOrderProducts(OrderProducts neworderproducts)
        {
            Repo.OrderProductsRepository.Insert(neworderproducts);
        }
        public void ModifyOrder(int id, int orderstateid)
        {
            Repo.OrderRepository.Modify(id, orderstateid);
        }

        public int GetNextOrderId()
        {
            return Repo.OrderRepository.GetAll().Max(x => (int)x.ID);
        }

        #endregion

        #region legujabb jatekok
        public IQueryable<ProductsLowPrice> GetNewestProducts()
        {
            var result = (from products in Repo.ProductRepository.GetAll()
                          orderby products.ReleaseDate ascending
                          select new ProductsLowPrice
                          {
                              ID = products.ID,
                              Name = products.Name,
                              Price = products.Price,
                              image1 = products.image1
                         }).Take(4);
            return result;
        }
        #endregion

        #region GetTwoGames
        public IQueryable<ProductsLowPrice> GetTwoGames()
        {
            var result = (from products in Repo.ProductRepository.GetAll()
                          select new ProductsLowPrice
                          {
                              ID = products.ID,
                              Name = products.Name,
                              Price = products.Price,
                              image1 = products.image1,
                              Description = products.Description
                          }).Take(2);
            return result;
        }
        #endregion

        #region GetThreeGames
        public IQueryable<ProductsLowPrice> GetThreeGames()
        {
            var result = (from products in Repo.ProductRepository.GetAll()
                          select new ProductsLowPrice
                          {
                              ID = products.ID,
                              Name = products.Name,
                              Price = products.Price,
                              image1 = products.image1
                          }).Take(3);
            return result;
        }
        #endregion
    }
}
