﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class GetOrderInformations
    {
        public int OrderID { get; set; }
        public int OrderStateID { get; set; }
        public string OrderStateName { get; set; }

        public string CusFirstName { get; set; }
        public string CusLastName { get; set; }
        public string CusEmail { get; set; }
        public string CusPhone { get; set; }
        public int CusZipCode { get; set; }
        public string CusTown { get; set; }
        public string CusStreet { get; set; }
        public int CusStreetNumber { get; set; }
    }
}
