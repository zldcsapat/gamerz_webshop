﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class ProductsInPlatform
    {
        public int ID { get; set; }
        public int PlatformID { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public byte[] image1 { get; set; }
    }
}
