﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class GetUsersAndJobTypes
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string E_mail { get; set; }
        public string Password { get; set; }
        public int JobTypeID { get; set; }
        public string JobTypeName { get; set; }  
    }

}
