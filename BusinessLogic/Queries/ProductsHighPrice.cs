﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class ProductsHighPrice
    {
        public int ID { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public byte[] image1 { get; set; }
    }
}
