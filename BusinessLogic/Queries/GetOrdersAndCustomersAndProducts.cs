﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class GetOrdersAndCustomersAndProducts
    {
        public int OrderID { get; set; }
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public byte[] image1 { get; set; }
        public int SubTotal { get; set; }
        public int Quantity { get; set; }
        public int TotalPrice { get; set; }

    }
}
