﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class GetWishlistGame
    {
        public int WishlistID { get; set; }
        public int GameID { get; set; }
        public int CustomerID { get; set; }
        public string GameName { get; set; }
        public int GamePrice { get; set; }
        public string Description { get; set; }
        public byte[] Image1 { get; set; }
    }
}
