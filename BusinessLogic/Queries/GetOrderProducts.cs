﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Queries
{
    public class GetOrderProducts
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public DateTime ProductReleaseDate { get; set; }
        public int ProductPrice { get; set; }
        public int ProductQuantity { get; set; }
        public byte[] ProductImage { get; set; }
        public string PlatformName { get; set; }
    }
}
